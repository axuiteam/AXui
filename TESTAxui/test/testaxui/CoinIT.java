/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class CoinIT {
    
   

    /**
     * Test of useByHero method, of class Coin.
     */
    @Test
    public void testUseByHero() {
        System.out.println("useByHero");
        Hero hero = new Hero("hero");
        
        
        Bag b=new Bag(10,20);
        assertEquals(hero.getBag(),b);
        
        
    }

    /**
     * Test of takeByHero method, of class Coin.
     */
    @Test
    public void testTakeByHero() {
        System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Coin instance = new Coin(10);
        
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
    }

    /**
     * Test of display method, of class Coin.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int value=10;
        int volume=1;
        Coin instance = new Coin(value);
       
       assertEquals("Error description",instance.getDesc(),"A coin, increase your balance &  use it to exchange items with other characters");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Coin");
       assertEquals("Error name ",instance.getName(),"C-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
        
    }

    /**
     * Test of simpleDisplay method, of class Coin.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("SimpleDisplay");
        int value=10;
        int volume=1;
        Coin instance = new Coin(value);
       
       
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Coin");
       assertEquals("Error name ",instance.getName(),"C-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
       
    }
    
}
