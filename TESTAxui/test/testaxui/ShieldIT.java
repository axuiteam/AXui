/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class ShieldIT {
    

    
    /**
     * Test of takeByHero method, of class Shield.
     */
    @Test
    public void testTakeByHero() {
         System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Shield instance=new Shield(5,10,6);
        
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
    }

    /**
     * Test of useByHero method, of class Shield.
     */
    @Test
    public void testUseByHero() {
        System.out.println("useByHero");
        Hero hero = new Hero("hero");
        
        
        Bag b=new Bag(10,20);
        assertEquals(hero.getBag(),b);
    }

    /**
     * Test of getSP method, of class Shield.
     */
    @Test
    public void testGetSP() {
        System.out.println("getSP");
        int sp=5;
        Shield instance=new Shield(sp,10,6);
        assertEquals(instance.getSP(),sp);
    }

    /**
     * Test of display method, of class Shield.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int sp=5;
        int value=10;
        int volume=6;
        Shield instance = new Shield(sp,value,volume);
       
       assertEquals("Error description",instance.getDesc(),"A shield,can refill your shieldpoint");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Shield");
       assertEquals("Error name ",instance.getName(),"SH-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }

    /**
     * Test of simpleDisplay method, of class Shield.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        int sp=5;
        int value=10;
        int volume=6;
        Shield instance = new Shield(sp,value,volume);
        assertEquals("Error name's class",instance.getClass().getSimpleName(),"Shield");
       assertEquals("Error name ",instance.getName(),"SH-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }
    
    
}
