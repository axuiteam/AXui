/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class HealIT {
    
    
    /**
     * Test of takeByHero method, of class Heal.
     */
    @Test
    public void testTakeByHero() {
        System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Heal instance=new Heal(5,10,6);
        
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));}

    /**
     * Test of useByHero method, of class Heal.
     */
    @Test
    public void testUseByHero() {
       System.out.println("useByHero");
        Hero hero = new Hero("hero");
        
        
        Bag b=new Bag(10,20);
        assertEquals(hero.getBag(),b);
    }

    /**
     * Test of getHealValue method, of class Heal.
     */
    @Test
    public void testGetHealValue() {
        System.out.println("getHealValue");
        int heal=5;
        Heal instance=new Heal(heal,10,6);
        assertEquals(instance.getHealValue(),heal);
        
    }

    /**
     * Test of display method, of class Heal.
     */
    @Test
    public void testDisplay() {
       System.out.println("display");
        int hv=5;
        int value=10;
        int volume=6;
        Heal instance = new Heal(hv,value,volume);
       
       assertEquals("Error description",instance.getDesc(),"A heal,can refill your healthpoint");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Heal");
       assertEquals("Error name ",instance.getName(),"H-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }

    /**
     * Test of simpleDisplay method, of class Heal.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
         int hv=5;
        int value=10;
        int volume=6;
        Heal instance = new Heal(hv,value,volume);
        
        assertEquals("Error name's class",instance.getClass().getSimpleName(),"Heal");
       assertEquals("Error name ",instance.getName(),"H-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }
    
}
