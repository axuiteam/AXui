/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class KnifeIT {
    
    
    /**
     * Test of SpecialByHero method, of class Knife.
     */
    @Test
    public void testSpecialByHero() {
        
    }

    /**
     * Test of display method, of class Knife.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
         int damage=5;
         int volume= 1;  
         int value= 0;
        Knife instance=new Knife(damage,value,volume);
       
       assertEquals("Error description",instance.getDesc(),"A knife, be brave and attack your target");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Knife");
       assertEquals("Error name ",instance.getName(),"KN-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
       assertEquals(instance.getDamage(),damage);
    }

    /**
     * Test of simpleDisplay method, of class Knife.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
         int damage=5;
         int volume= 1;  
         int value= 0;
        Knife instance=new Knife(damage,value,volume);
       
       
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Knife");
       assertEquals("Error name ",instance.getName(),"KN-0");
       assertEquals(instance.getDamage(),damage);
    }
    
}
