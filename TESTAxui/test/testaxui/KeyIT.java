/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class KeyIT {
    
    
    /**
     * Test of useByHero method, of class Key.
     */
    @Test
    public void testUseByHero() {
    }

    /**
     * Test of takeByHero method, of class Key.
     */
    @Test
    public void testTakeByHero() {
         System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Key instance=new Key("key1",10,6);
        
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));;
    }

    /**
     * Test of display method, of class Key.
     */
    @Test
    public void testDisplay() {
       System.out.println("display");
         
         String name="key1";
         int volume= 3;  
         int value= 50;
        Key instance=new Key(name,value,volume);
       
       assertEquals("Error description",instance.getDesc(),"A key, there's somewhere a door that I can open");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Key");
       assertEquals("Error name ",instance.getName(),name);
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }

    /**
     * Test of simpleDisplay method, of class Key.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        
         String name="key1";
         int volume= 3;  
         int value= 50;
        Key instance=new Key(name,value,volume);
       
       
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Key");
       assertEquals("Error name ",instance.getName(),name);
       assertEquals(instance.getVolume(),volume);
    }
    
}
