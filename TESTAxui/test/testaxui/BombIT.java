/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class BombIT {
    
    

    /**
     * Test of getSideEffectDamage method, of class Bomb.
     */
    @Test
    public void testGetSideEffectDamage() {
        System.out.println("getSideEffectDamage");
        int SideEffectDamage=5;
        Bomb instance = new Bomb(SideEffectDamage,10,20,6);
        assertEquals(instance.getSideEffectDamage(),SideEffectDamage);
        
    }

    /**
     * Test of SpecialByHero method, of class Bomb.
     */
    @Test
    public void testSpecialByHero() {
        
    }

    /**
     * Test of display method, of class Bomb.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
         int damage=10;
         int volume= 6;  
         int value= 20;
        int SideEffectDamage=5;
        Bomb instance = new Bomb(SideEffectDamage,damage,value,volume);
       
       assertEquals("Error description",instance.getDesc(),"Great damage bomb to your opponents, use it with caution");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Bomb");
       assertEquals("Error name ",instance.getName(),"B-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
       assertEquals(instance.getDamage(),damage);
       assertEquals(instance.getSideEffectDamage(),SideEffectDamage);
    }

    /**
     * Test of simpleDisplay method, of class Bomb.
     */
    @Test
    public void testSimpleDisplay() {
         System.out.println("SimpleDisplay");
         int damage=10;
         int volume= 6;  
         int value= 20;
        int SideEffectDamage=5;
        Bomb instance = new Bomb(SideEffectDamage,damage,value,volume);
       
       
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Bomb");
       assertEquals("Error name ",instance.getName(),"B-0");
       assertEquals(instance.getVolume(),volume);
       assertEquals(instance.getDamage(),damage);
       assertEquals(instance.getSideEffectDamage(),SideEffectDamage);
        
    }
    
}
