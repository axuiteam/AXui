/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package testaxui;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class OtherIT {
    
   

    /**
     * Test of getMessage method, of class Other.
     */
    @Test
    public void testGetMessage() {
        System.out.println("getMessage");
        String mess="you can use this paper to figure out the password";
        Other instance =new Other(mess,"paper",1);
        assertEquals(instance.getMessage(),mess);
        
    }

    /**
     * Test of useByHero method, of class Other.
     */
    @Test
    public void testUseByHero() {
        
    }

    /**
     * Test of takeByHero method, of class Other.
     */
    @Test
    public void testTakeByHero() {
        System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Other instance=new Other("you can use this paper to figure out the password","paper",1);
        
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
    }

    /**
     * Test of display method, of class Other.
     */
    @Test
    public void testDisplay() {
         System.out.println("display");
         String msg ="you can use this paper to figure out the password";
         String name="paper";
         int volume= 1;  
         int value= 0;
        Other instance=new Other(msg,name,volume);
       
       assertEquals("Error description",instance.getDesc(),"May be useful");
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Other");
       assertEquals("Error name ",instance.getName(),name);
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }

    /**
     * Test of simpleDisplay method, of class Other.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("display");
         String msg ="you can use this paper to figure out the password";
         String name="paper";
         int volume= 1;  
         int value= 0;
        Other instance=new Other(msg,name,volume);
       
       
       assertEquals("Error name's class",instance.getClass().getSimpleName(),"Shield");
       assertEquals("Error name ",instance.getName(),"SH-0");
       assertEquals(instance.getValue(),value);
       assertEquals(instance.getVolume(),volume);
    }
    
}
