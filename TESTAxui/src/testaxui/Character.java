
package testaxui;


public abstract class Character extends Bot {
    
    private boolean isEvil; 
    public Character(String name, int hp, String DESC,String MSG, boolean isEvil) {
       super(name, hp, DESC,MSG);
       this.isEvil = isEvil;
    }
    
    @Override
    
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }



    @Override
    public void attackedByHero(Hero hero) {
        super.hp -=hero.getDamage();
        if(this.dead(hero)){
            hero.getCurentRoom().addMonster(new GhostOfJustice("jUsTiCe") );
        }
    }
    
}
