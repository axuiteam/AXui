/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testaxui;


public class GhostOfJustice extends Monster {
    private static final int DEFAULT_HP = 999999;
    private static final int DEFAULT_DAMAGE = 999999;
    private static final String GhostOfJustice_DESC="";
    public GhostOfJustice( String Name) {

        super(Name, DEFAULT_HP, GhostOfJustice_DESC, DEFAULT_DAMAGE);
        
    }
    
   
}

