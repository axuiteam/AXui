package testaxui;

import java.util.HashMap;
import java.util.Map;

public class LockedWithKey extends Exit {

	private final Key key;
	private static Map<Key, LockedWithKey> MapKeys;
        private static final boolean DEFAULT_IS_OPEN = false;
	private static final boolean DEFAULT_IS_LOCKED = true;
        private static final String WITHKEY_DESC="You need a key to open this door";
	/**
	 * 
	 * @param name
	 * @param desc
     * @param dest
	 * @param key
	 */
        static{
            LockedWithKey.MapKeys=new HashMap<>();
        }
	public LockedWithKey(String name,String dest, Key key){
		super(WITHKEY_DESC,name,LockedWithKey.DEFAULT_IS_OPEN,LockedWithKey.DEFAULT_IS_LOCKED,dest);
                
                if(!LockedWithKey.MapKeys.containsKey(key))
                {    LockedWithKey.MapKeys.put(key, this);
                    this.key=key;
                }
                else{
                    key = new Key(key.getName(),key.getValue(),key.getVolume());
                    LockedWithKey.MapKeys.put(key, this);
                    this.key=key;
                }
                
	}
        

	public boolean open() {
		if(!super.isLocked())
                    return true;
                else{
                System.out.println("you can't open this door,USE <Key> <Exit>  END");
                return false;
                    }
                    
                }
	

	public void keyUnLock(Key key) {
		if(this.isMyKEY(key))
                    super.unLock();
	}

	/**
	 * 
	 * @param key
	 */
	public boolean isMyKEY(Key key) {
		if(key == this.key)
                    return true;
                return false;
	}

    @Override
    public String getDesc() {
        return this.getClass().getSimpleName() +"\t"+ super.getDesc();
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Destination : "+this.getDestination());
        System.out.println("\t\t Description : "+this.getDesc());
        String str =(super.isLocked())?"LOCKED":"UNLOCKED";
        System.out.println("\t\t  "+str);
    }
    
        @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Destination : "+this.getDestination());
        String str =(super.isLocked())?"LOCKED":"UNLOCKED";
        System.out.println("\t\t  "+str);
    }



}