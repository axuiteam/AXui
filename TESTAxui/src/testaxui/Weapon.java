
package testaxui;


public abstract class Weapon extends Item {
    
    private final int DAMAGE;
    
     public Weapon(int damage,String NAME,int VALUE,int VOLUME,String DESC){
        super(NAME,VALUE,VOLUME,DESC);
        this.DAMAGE=damage;
    }
     
     public int getDamage(){
         return this.DAMAGE;
     }
    
     public abstract void SpecialByHero(); 
     
    @Override
      public void useByHero(Hero hero,String Arg){
       if(!hero.hasWeapon())
        {   
            hero.setEquipped(this);
            hero.getBag().removeItem(this);
        }else
       {
           if(hero.getBag().addItem(hero.getEquipped()))
           {
               hero.setEquipped(this);
               hero.getBag().removeItem(this);
               System.out.println("switch Weapons");
           }else
           {
               hero.dropItem(hero.getEquipped().getName());
               hero.getBag().removeItem(this);
              
           }
           
       }
       
     
    }

    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {if(hero.getBag().addItem(this))
                hero.getCurentRoom().removeItem(this.getName());
         }else{
                System.out.println("you don't have a bag");
            }
         
         
    }
}
