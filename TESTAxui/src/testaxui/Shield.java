
package testaxui;


public class Shield extends Item{
    
    private final int SP;
    private static int namIndx=0;
    private static final String SHIELD_DESC="A shield,can refill your shieldpoint";
    public Shield(int SP,int VALUE,int VOLUME){
        super("SH-"+Shield.namIndx,VALUE,VOLUME,SHIELD_DESC);
        this.SP=SP;
    }

    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {if(hero.getBag().addItem(this))
                hero.getCurentRoom().removeItem(this.getName());
         }else{
                System.out.println("you don't have a bag");
            }
         
         
    }

    @Override
    public void useByHero(Hero hero,String Arg) {
        hero.increaseShield(this.getSP());
        hero.getBag().removeItem(this);
        
    }
    public int getSP(){return this.SP;}

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t SP : "+this.getSP());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t SP : "+this.getSP());
    }
    
}
