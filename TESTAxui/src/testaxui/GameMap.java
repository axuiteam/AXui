package testaxui;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class GameMap {

	public static Map<String,Place> rooms;
        
        static{
            GameMap.rooms = new HashMap<>();
            
            List<Exit> exits = new ArrayList<>();
            List<Item> items = new ArrayList<>();
            List<Monster> mons = new ArrayList<>();
            List<Character> caras = new ArrayList<>();
            items.add(new Coin(10));
            items.add(new Coin(20));
            items.add(new Bag(150,500));
            exits.add(new OpenExit("DOOR-TO-GEAR_ROOM","GEAR_ROOM"));
            exits.add(new OpenExit("DOOR-TO-FIRST_CHALLENGE","FIRST_CHALLENGE"));
            Place temp = new Place("START_HALL",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            items.add(new Knife(200,110,100));
            items.add(new Heal(300,50,30));
            items.add(new Shield(1000,200,30));
            items.add(new Other("take all you can","BE_READY",1));
            items.add(new Bomb(60,700,250,30));
            exits.add(new OpenExit("DOOR-TO-START_HALL","START_HALL"));
            temp = new Place("GEAR_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-START_HALL","START_HALL"));
            exits.add(new LockedWithPasword("DOOR-TO-DECISION_ROOM","DECISION_ROOM"));
            Key k=new Key("Key DECISION_ROOM",2,2);
            exits.add(new LockedWithKey("DOOR-TO-GREEN_HOUSE","GREEN_HOUSE",k));
            temp = new Place("FIRST_CHALLENGE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-FIRST_CHALLENGE","FIRST_CHALLENGE"));
            temp = new Place("GREEN_HOUSE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-FIRST_CHALLENGE","FIRST_CHALLENGE"));
            exits.add(new OpenExit("DOOR-TO-LEFT_SIDE","LEFT_SIDE"));
            exits.add(new OpenExit("DOOR-TO-RIGHT_SIDE","RIGHT_SIDE"));
            temp = new Place("DECISION_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            

            
            
            
            //leftside
            
            exits = new ArrayList<>();
            
            exits.add(new OpenExit("DOOR-TO-L_BALCONY","L_BALCONY"));
            temp = new Place("LEFT_SIDE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-LEFT_SIDE","LEFT_SIDE"));
            exits.add(new OpenExit("DOOR-TO-ADMIN","ADMIN"));
            temp = new Place("L_BALCONY",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-L_BALCONY","L_BALCONY"));
            exits.add(new OpenExit("DOOR-TO-SUICIDE_ROOM","SUICIDE_ROOM"));
            exits.add(new OpenExit("DOOR-TO-COWARDS","COWARDS"));
            temp = new Place("ADMIN",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();

            exits.add(new OpenExit("DOOR-TO-LAIRS_ROOM","LAIRS_ROOM"));
            exits.add(new OpenExit("DOOR-TO-ADMIN","ADMIN"));
            temp = new Place("COWARDS",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-ADMIN","ADMIN"));
            exits.add(new OpenExit("DOOR-TO-RECOVER","RECOVER"));
            temp = new Place("SUICIDE_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            
            
            //RIGHTSIDE
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-R_BALCONY","R_BALCONY"));
            temp = new Place("RIGHT_SIDE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-RIGHT_SIDE","RIGHT_SIDE"));
            exits.add(new OpenExit("DOOR-TO-PIGS_ROOM","PIGS_ROOM"));
            temp = new Place("R_BALCONY",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-LAIRS_ROOM","LAIRS_ROOM"));
            temp = new Place("PIGS_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-X","X"));
            exits.add(new OpenExit("DOOR-TO-Y","Y"));
            exits.add(new OpenExit("DOOR-TO-Z","Z"));
            exits.add(new OpenExit("DOOR-TO-W","W"));
            temp = new Place("LAIRS_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-JUDGMENT","JUDGMENT"));
            temp = new Place("W",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            temp = new Place("Z",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            temp = new Place("X",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            temp = new Place("Y",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-SUICIDE_ROOM","SUICIDE_ROOM"));
            exits.add(new OpenExit("DOOR-TO-JUDGMENT","JUDGMENT"));
            temp = new Place("RECOVER",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            exits.add(new OpenExit("DOOR-TO-THE_END","THE_END"));
            temp = new Place("JUDGMENT",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-THE_END","THE_END"));
            temp = new Place("HELL",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            temp = new Place("THE_END",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
        }
        public static void showMap()
        {
            GameMap.rooms.forEach((String k,Place v)->{
                System.out.println(k+"\n");
                v.display();
            });
        }
        public static Place getPlace(String place)
        {
            
            return GameMap.rooms.get(place);
        }

}