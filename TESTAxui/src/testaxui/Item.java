
package testaxui;


public abstract class Item extends Observable implements Takebale,Usable{
        private final String NAME;
	private final int VALUE;
	private final int VOLUME;
        
        public Item(String NAME,int VALUE,int VOLUME,String DESC){
            super(DESC);
            this.NAME=NAME;
            this.VALUE=VALUE;
            this.VOLUME=VOLUME;
        }
        
        @Override 
        public String getDesc(){
            return DESC;
        }
        
        
       public String getName(){
           return this.NAME;
       }
       
       public int getVolume(){
           return this.VOLUME;
       }
       
        public int getValue(){
           return this.VALUE;
       }

    
    
}
