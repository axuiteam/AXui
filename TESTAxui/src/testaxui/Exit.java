package testaxui;

public abstract class Exit extends Observable {

	private boolean isOpen;
	private boolean isLocked;
	private final String NAME;
        private final String DESTINATION;   


	/**
	 * 
	 * @param desc
	 * @param Name
	 * @param isOpen
	 * @param isLocked
	 */
	public Exit(String desc, String Name, boolean isOpen, boolean isLocked,String dest) {
		super(desc);
		this.NAME=Name;
                this.isLocked=isLocked;
                this.isOpen=isOpen;
                this.DESTINATION=dest;
	}
        
        protected boolean isOpen(){return this.isOpen;}
        protected boolean isLocked(){return this.isLocked;}
	public String getName() {
		return this.NAME;
	}
        public String getDestination() {
		return this.DESTINATION;
	}

	public abstract boolean open();

	protected void unLock(){
            this.isLocked=false;
        }

        public abstract void simpleDisplay();
        

}