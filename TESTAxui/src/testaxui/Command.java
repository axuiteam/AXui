package testaxui;

public enum Command {
	DROP,
	GO,
	HELP,
	LOOK,
	TAKE,
	USE,
	ATTACK,
	SPEAK,
        HERO,
	PAUSE,
	QUIT;       
}