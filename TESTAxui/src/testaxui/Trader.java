
package testaxui;

import java.util.*;


public class Trader extends Character implements Buyer,Seller {
    private static final boolean IS_EVIL = false;
    private List <Item> traderItems;
    private static final String TRADER_DESC=""; 

    public Trader( String Name, int hp, List<Item> objects) {
        super(Name, hp,TRADER_DESC,"Trader",IS_EVIL);   
        this.traderItems  = objects;
    }   
    
    
    @Override    
    public void buyFromHero(Hero hero){
         System.out.println(super.getMessage()+" "+this.getName()+" : name what do you want sell");
         hero.getBag().getItms().forEach((k,v)->
         {
             System.out.println(k +":"+ v.getValue()/2 );

         });
         String s;
         Scanner sc = new Scanner(System.in);
         System.out.print(">>");
          try{
                s=sc.next();
                }
          catch(Exception e)
                {
                    sc.nextLine();
                    s="";
                }
          if(hero.getBag().hasItem(s))
          {
              
              hero.increaseBalance(hero.getBag().getItem(s).getValue()/2);
              this.traderItems.add(hero.getBag().getItem(s));
              hero.getBag().removeItem(hero.getBag().getItem(s));
          }
          else
          {
              System.out.println("mmm ok bye");
          }
    }
    
    @Override
    public void sellToHero(Hero hero){
        System.out.println("select your choice :");
        System.out.println("Index   NAME");
        int i=1;
        for(Item e : this.traderItems){
            System.out.println(i+" : "+ e.getClass().getSimpleName() +" "+e.getValue() );
            i++;
        }
        
        
        Scanner sc = new Scanner(System.in);
        System.out.print(">>");
        try{
                i=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    i=0;
                }
        i--;
        if(i>0 && i < this.traderItems.size())
        {
            hero.getCurentRoom().addItem(this.traderItems.get(i));
            hero.takeItem(this.traderItems.get(i).getName());
            this.traderItems.remove(i);
        }
        
    }

    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(super.getMessage()+" "+this.getName()+" : select your choice");
        System.out.println("\t\t 1-Buy");
        System.out.println("\t\t 2-sell");
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print(">>");
                
                try{
                n=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    n=0;
                }
                System.out.println("");   
                switch(n)
                {   
                    case 1:{System.out.println("");
                        
                        this.sellToHero(hero);
                        
                        break;
                    }
                    case 2:{
                        this.buyFromHero(hero);
                        break;
                    }
                    default:{
                        System.out.println("mmm ok bye");
                        
                    }
                }
        
        
    }
    
    
}
