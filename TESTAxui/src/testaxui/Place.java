package testaxui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Place extends Observable {

	private static final String PLACE_DESC="A room, where you can find useful items, be careful sometimes there are monsters."; 
        private Hero hero;
        Map<String,Item> roomItems;
	Map<String,Exit> exits;
        Map<String,Monster> monsters;
        Map<String,Character> caracters;
	private String NAME;

	/**
	 * 
     * @param desc
	 * @param exits
	 * @param name
	 */
	public Place(String name,List<Exit> exits,List<Item> items,List<Monster> mon,List<Character> car) {
            super(PLACE_DESC);
            this.NAME=name;
            this.exits= new HashMap<>();
            this.roomItems= new HashMap<>();
            this.monsters=new HashMap<>();
            this.caracters=new HashMap<>();
            exits.forEach(e -> {
                this.exits.put(e.getDestination(), e);
            });
            items.forEach(i -> {
                this.roomItems.put(i.getName(), i);
            });
            mon.forEach(e->{
                this.monsters.put(e.getName(), e);
            });
            car.forEach(e->{
                this.caracters.put(e.getName(), e);
            });
	}


    @Override
    public String getDesc() {
        return this.getClass().getSimpleName()+"\t" + super.getDesc();
    }
    public String getName()
    {
        return this.NAME;
    }
    

    @Override
    public void display() {
        
        System.out.println("\tName : "+this.getName());
        System.out.println("\texits : ");
        this.exits.forEach((String k,Exit v)->
        {
            System.out.print("\t\t");
            v.simpleDisplay();
        }              
        );
        System.out.println("\titems : ");
        this.roomItems.forEach((String k,Item v)->
        {
            System.out.print("\t\t");
            v.display();
        }              
        );
    }
    public void simpleDisplay() {
       System.out.println("\tName : "+this.getName());
       System.out.println("\texits : ");
       this.exits.forEach((String k,Exit v)->
        {
            System.out.print("\t\t");
            v.simpleDisplay();
        }         
        );
        System.out.println("room has : "+this.roomItems.size()+" items "
                + "for more details use the command <LOOK AROUND END>");
    }

    public void setHero(Hero hero)
    {
        this.hero=hero;
    }
    
    public boolean hasExit(String exit){
        return this.exits.containsKey(exit);
        
    }

    public Exit getExit(String exit)
    {
        return this.exits.get(exit);
    }
    public boolean hasItem(String item){
        return this.roomItems.containsKey(item);
        
    }

    public Item getItem(String item)
    {
        return this.roomItems.get(item);
    }
    public void removeItem(String i)
    {
        this.roomItems.remove(i);
    }
    public void addItem(Item i)
    {
        this.roomItems.put(i.getName(), i);
    }
    
    public void showItem(String item)
    {
        this.roomItems.get(item).display();
        
    }
    
    public void removeBot(String s)
    {
        if(this.caracters.containsKey(s))
            this.caracters.remove(s);
        if(this.monsters.containsKey(s))
            this.monsters.remove(s);
    }
    public void addMonster(Monster m)
    {    
        this.monsters.put(m.getName(), m);
    }
    
    public boolean hasMonsters()
    {
        if(this.monsters.size()>0)
            return true;
        return false;
    }
    
    public void AttackHero()
    {
        if(this.hero != null)
            this.monsters.forEach((String k,Monster v)->{
                v.attackHero(this.hero);
            });
        
    }
}