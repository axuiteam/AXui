
package testaxui;


public class Monster extends Bot implements AttackHero{
    
    private int damage;
    private static final String MSG="ARGH ARGH ARGH GHHHHH";
    private static final String MONSTER_DESC=""; 
    public Monster(String name, int hp, String description, int damage) {
        super(name, hp, description, Monster.MSG);
	this.damage = damage; 
    }
    public Monster(String name, int hp,  int damage) {
        super(name, hp, MONSTER_DESC, Monster.MSG);
	this.damage = damage; 
    }
	
    public int getDamage() {
	return this.damage;
    }
	
    @Override 
    public void attackHero(Hero hero) {
        hero.decreaseHp_Shield(this.damage);
    }
        
    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }

    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(super.getName()+"\" "+super.getMessage()+"\"");
    }

    @Override
    public void attackedByHero(Hero hero) {
        super.hp -=hero.getDamage();
        this.dead(hero);
    }

    
        
}
 