
package testaxui;

import java.util.ArrayList;
import java.util.List;

public abstract class Bot extends Observable{
    
    private final String NAME;
    protected  int hp;
    private String MESSAGE;
    private List<Item> items;
    public Bot(String name, int hp, String DESC,String msg) {
                super(DESC);
                this.MESSAGE=msg;
		this.NAME = name;
		this.hp = hp;
                this.items=new ArrayList();
	}
	
	public String getName() {
		return this.NAME;
	}
	
	public int getHp() {
		return this.hp;
	}
        public String getMessage(){
            return this.MESSAGE;
        }
	
	@Override 
        public String getDesc(){
            return DESC;
        }
        
        public void addItem(Item item)
        {
            this.items.add(item);
        }
	
        public void dropItems(Place place)
        {
            this.items.forEach(i->place.addItem(i));
        }
        
        public boolean isAlive()
        {
            if(this.hp>0){
            return true;
            }
            return false;
        }
        
        public boolean dead(Hero hero)
        {
            if(this.isAlive())
            {
                this.dropItems(hero.getCurentRoom());
                hero.getCurentRoom().removeBot(this.getName());
                return true;
            }
            return false;
        }
        
	public abstract void speackWithHero(Hero hero);
	
	public abstract void attackedByHero(Hero hero);
}
