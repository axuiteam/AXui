package testaxui;
import java.util.Random;
import java.util.Scanner;

public class LockedWithPasword extends Exit {

	private final String PASSWORD;
        private static final String WITHKEY_DESC="You need a key to open this door";
        private static final boolean DEFAULT_IS_OPEN = false;
	private static final boolean DEFAULT_IS_LOCKED = true;
        private static final String WITHPASSWORD_DESC="You need a PASSWORD to open this door";
	/**
	 * 
	 * @param name
	 * @param desc
	 * @param password
	 */
	public LockedWithPasword(String name,String dest)
        {
            super(WITHPASSWORD_DESC,name,LockedWithPasword.DEFAULT_IS_OPEN,LockedWithPasword.DEFAULT_IS_LOCKED,dest);
            this.PASSWORD=getRandomPassword(6);
	}

	public boolean open() {
		if(!super.isLocked())
                    return true;
                else{
                    System.out.print("password >> ");
                    Scanner sc= new Scanner(System.in);
                    if(this.isMyPassword(sc.nextLine()))
                    {
                        System.out.println("Correct password");
                        this.unLock();
                        return true;
                    }else
                    {
                        System.out.println("incorrecet password");
                        return false;
                    }
                    
                }
	}

        @Override
	public void unLock() {
		super.unLock();
	}

	/**
	 * 
	 * @param password
	 */
	public boolean isMyPassword(String password) {
		if(password.equals(this.PASSWORD))
                    return true;
                else
                    return false;
	}
        public static String getRandomPassword(int n)
        {
            char sym[]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9'};
            Random r= new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++)
                sb.append(sym[r.nextInt(sym.length)]);
            return sb.toString();
        }

    

        @Override
    public String getDesc() {
        return this.getClass().getSimpleName() +"\t"+ super.getDesc();
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Destination : "+this.getDestination());
        System.out.println("\t\t Description : "+this.getDesc());
        String str =(super.isLocked())?"LOCKED":"UNLOCKED";
        System.out.println("\t\t  "+str);
        //delete this ..
        System.out.println("\t\t Password : "+this.PASSWORD);
    }
    
        @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Destination : "+this.getDestination());
        String str =(super.isLocked())?"LOCKED":"UNLOCKED";
        System.out.println("\t\t  "+str);
        //delete this ..
        System.out.println("\t\t Password : "+this.PASSWORD);
    }

}