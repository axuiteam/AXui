
package testaxui;


public class Coin extends Item implements Usable,Takebale{
    
    private static final int COIN_VOLUME=1; 
    private static int namIndx=0;
    private static final String COIN_DESC="A coin, increase your balance &  use it to exchange items with other characters"; 
    public Coin(int VALUE){
        super("C-"+Coin.namIndx,VALUE,Coin.COIN_VOLUME,COIN_DESC);
    }
 
    @Override
    public void useByHero(Hero hero,String Arg){
        hero.increaseBalance(this.getValue());
        hero.getBag().removeItem(this);
        
          
    }

    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {if(hero.getBag().addItem(this))
                hero.getCurentRoom().removeItem(this.getName());
         }else{
                System.out.println("you don't have a bag");
            }
         
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
    }
    
}
