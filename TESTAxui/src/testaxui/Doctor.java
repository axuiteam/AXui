
package testaxui;

import java.util.List;
import java.util.Scanner;


public class Doctor extends Character implements Seller{
    private static final boolean IS_EVIL = false;
    private List <Heal> DoctorItems;
    private static final String DOCTOR_DESC="";
    
    public Doctor(String Name, int hp, List <Heal> heals) {
        super(Name,hp,DOCTOR_DESC,"DOCTOR :", IS_EVIL);   
        heals.forEach(e->{
            this.DoctorItems.add(e);
        });
        
    }
    
    @Override
    
     public void sellToHero( Hero hero){
        
      System.out.println("select your choice :");
        System.out.println("Index   NAME");
        int i=1;
        for(Item e : this.DoctorItems){
            System.out.println(i+" : "+ e.getClass().getSimpleName() +" "+e.getValue() );
            i++;
        }
        
        
        Scanner sc = new Scanner(System.in);
        System.out.print(">>");
        try{
                i=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    i=0;
                }
        i--;
        if(i>0 && i < this.DoctorItems.size())
        {
            hero.getCurentRoom().addItem(this.DoctorItems.get(i));
            hero.takeItem(this.DoctorItems.get(i).getName());
            this.DoctorItems.remove(i);
        }
    
}

    @Override
    public void speackWithHero(Hero hero) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
