
package testaxui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Bag extends Item {
    private int storage;//current storage
    private final int CAPACITY;
    private final static int DEFAULT_STORAGE = 0;
    private Map <String,Item> objects;
    private static int namIndx=0;
    private static final String BAG_DESC="A bag,  stock  your items whenever there is enough space"; 
  
    public Bag(int VALUE,int VOLUME){
        super("BG-"+Bag.namIndx,VALUE,VOLUME,BAG_DESC);
        this.CAPACITY = VOLUME - 3;
        this.storage = DEFAULT_STORAGE;
        this.objects =  new HashMap();
    }



    public int getFreeSpace() {
        return this.CAPACITY - this.storage ;
    }  
  
    public boolean hasEnoughSpace(Item item){
        if(item.getVolume()<=this.getFreeSpace()){
            return true;
        }
        else return false;
    }
  
    public boolean addItem(Item item){
       
        if(hasEnoughSpace(item)){
            this.objects.put(item.getName(),item);
            this.storage+=item.getVolume();
            System.out.println("Item +");
            return true;
        }
        else System.out.println("you don't have enough space to add this item");
        return false;
    } 
    
    public void removeItem(Item item){
        this.storage-=item.getVolume();
        this.objects.remove(item.getName());
    }           
       //add place
    public void showItems(){
        System.out.println("\titems["+this.objects.size()+"] : ");
            
        this.objects.forEach((String k,Item i)->
        {
            System.out.print("\t\t");
        
            i.simpleDisplay();
        }
);
    }
 
    public void dropAllItem(Place place){
        this.objects.forEach((String k,Item i)->
        {
           place.addItem(i);
        });
        System.out.println(this.objects.size()+" Items -");
        this.storage=DEFAULT_STORAGE;
        this.objects.clear();
        
    }
    public void moveItems (Bag newbag){
        this.objects.forEach((String k,Item i)->
        {
            if(newbag.hasEnoughSpace(i)){
                newbag.objects.put(i.getName(),i);
                newbag.storage+=i.getVolume();
                this.objects.remove(k);
            }
        });
        
        
    }
    public String getDesc() {
        return this.getClass().getSimpleName()+"\t" + super.getDesc();
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\tCapacity[storage] : "+this.CAPACITY+"["+this.storage+"]");
        System.out.println("");
        showItems();
    }
    
    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {
             hero.getBag().moveItems(this);
             hero.getBag().dropAllItem(hero.getCurentRoom());
             hero.getBag().DropBag(hero.getCurentRoom());
             hero.setBag(this);
         }else{
         hero.setBag(this);
         hero.getCurentRoom().removeItem(this.getName());
         }
         
    }

    public void DropBag(Place place)
    {
        this.dropAllItem(place);
        place.addItem(this);
    }
    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\tCapacity[storage] : "+this.CAPACITY+"["+this.storage+"]");
        System.out.println("\titems["+this.objects.size()+"]");
        
    }

    @Override
    public void useByHero(Hero hero,String Arg) {
        throw new UnsupportedOperationException("Not supported ."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Item getItem(String s)
    {
        if(this.hasItem(s))
        return this.objects.get(s);
        return null;
    }

    boolean hasItem(String i) {
      return this.objects.containsKey(i);
    }
    public void dropItem(String i,Place place)
    {
        this.storage-=this.objects.get(i).getVolume();
        place.addItem(this.objects.get(i));
        this.removeItem(this.objects.get(i));
    }
    public Map<String,Item> getItms()
    {
        return this.objects;
    }
 
}

