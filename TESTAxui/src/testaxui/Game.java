package testaxui;

import java.util.Scanner;




public class Game {

    
    private Hero hero;
    
    public Game()
    {
        this.Axui();
        System.out.println(" ---------------------------------------------------- ");
        System.out.println("|xxxxx                AXui                      xxxxx|");
        System.out.println("");
        System.out.println("\t\t\t1-new Game");
        System.out.println("\t\t\t2-Records");
        System.out.println("\t\t\t3-About US");
        System.out.println("\t\t\t4-Quit");
        System.out.println("|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx|");
        System.out.println(" ---------------------------------------------------- ");
        System.out.println("");
      
        Scanner sc = new Scanner(System.in);
        int n;
        boolean b=true;
        while(b){
                System.out.print(">>");
                
                try{
                n=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    n=0;
                }
                System.out.println("");   
                switch(n)
                {   
                    case 1:{System.out.println("");
                        this.startGame();
                        b=false;
                        break;
                    }
                    case 2:{
                        this.records();
                        b=false;
                        break;
                    }
                    case 3:{
                        this.aboutUs();
                        b=true;
                        break;
                    }
                    case 4:{
                        this.quit();
                        break;
                    }
                    default :{
                        System.out.println("XxX INVALID XxX");
                        b=true;
                        break;
                    }
                }

    }
    }
    public void startGame()
    {
        
        this.Story();
        System.out.println("enter your Name :");
        String name ="";
        Scanner sc = new Scanner(System.in);
        while(name.isBlank()){
                System.out.print(">>");
                name=sc.nextLine();
        }
        this.hero=new Hero(name);
        this.hero.initCurentRoom("START_HALL");
        String com;
        while(this.hero.NotYet())
        {
            System.out.println("");
            System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
            System.out.print(">>");
            com = sc.nextLine();
            
            if(!Game.validComannd(com)){
                System.out.println("xxxxxxxxxxxxxxINVALID COMMANDxxxxxxxxxxxxxx");
                continue;
            }else{
                 
                 String[] Args=Game.getArgs(com);
                 Command c= Command.valueOf(Args[0]);
                 switch(c){
                     case QUIT:{
                         this.quit();
                         break;
                     }
                     case HERO: {
                         this.hero.ShowHero();
                         
                         break;
                     }
                     case GO :{
                         //if there is monsters sout 
                         if ((Args.length==3))
                         {
                             this.hero.go(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case LOOK :{
                         if ((Args.length==3))
                         {
                             this.hero.look(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case HELP :{
                         if(Args.length==2 || Args.length==3){
                            if ((Args.length==2))
                            {
                                this.Help();
                            }
                            if ((Args.length==3))
                            {
                                this.Help(Args[1]);
                            }
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case TAKE:
                     {
                         if ((Args.length==3))
                         {
                             this.hero.takeItem(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case DROP:
                     {
                         if ((Args.length==3))
                         {
                             this.hero.dropItem(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case USE:
                     {
                         if ((Args.length>=3 && Args.length<5))
                         {
                             
                             this.hero.useItem(Args);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                 }
            }
            System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
            System.out.println("");
            
        }
        this.END();
    
    }
    public void records(){}
    public void quit(){
        System.exit(0);
    }
    private static boolean validComannd(String str){
        str= str.toUpperCase();
        String [] coms = str.split(" ",0);
        if(coms.length <2 || !coms[coms.length-1].equals("END"))
        return false;
        try{
            Command c = Command.valueOf(coms[0]);
        }catch(Exception e){
            return false;
        }
        return true;
    }
    
    private static String[] getArgs(String str){
        str= str.toUpperCase();
        str.replace("END", "");
        String [] coms = str.split(" ",0);
        return coms;
    }
    private void Story() {
        String str = "I hope you’re ready to live such an exciting experience.\n"
                + "Enter strange and unexplored rooms full of creatures, dangers,\n"
                + "and surprises. Your only companion to face hardships is your magic bag,\n"
                + "and your main objective is to win the game by going through all the exits to arrive to the final room.";
        
        System.out.println(str);
        System.out.print("\npress any key >");
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {
        
        }
        System.out.println("");
    }
    private void aboutUs()
    {
        System.out.println("\t\tthe project will be published on November 15, 2021\n\t\t" +
"    GitLab link:https://gitlab.com/axuiteam/AXui");
    
    }
    private void END(){
        if((this.hero.getCurentRoom().getName()).equals("THE_END")){
            System.out.println("Congratulations, you  won the game");
            this.records();
        }else{
            System.out.println("xxxxxxxxxxxxxxxGAME OVER!xxxxxxxxxxxxxxx");
        }
    }
    
    private void Help(){
        System.out.println("\t\t\t-x COMMANDS LIST x-");
        System.out.println("\t\t+ HELP END : COMMANDS LIST ");
        System.out.println("\t\t+ HELP <COMMAND> END : HOW TO USE COMMAND ");
        System.out.println("\t\t+ HERO END : MAIN CHARACTER &  CURRENT LOCATION DATA");
        System.out.println("\t\t+ GO <DESTINATION> END : MOVE TO DESTINATION ROOM ");
        System.out.println("\t\t+ LOOK <ARG> END : ARG{<ITEM>,<BOT>,<EXIT(DESTINATION)>} DESCRIPTION OF <ARG> ");
        System.out.println("\t\t+ LOOK <Weapon> END : WEAPON DESCRIPTION ");
        System.out.println("\t\t+ LOOK <BAG> END : YOUR BAG DESCRIPTION ");
        System.out.println("\t\t+ LOOK AROUND END : YOUR CURRENT LOCATION DESCRIPTION");
        System.out.println("\t\t+ TAKE <ITEM> END : TAKE THE ITEM YOU WANT FROM CURRENT LOCATION");
        System.out.println("\t\t+ USE <ITEM> END : USE AN ITEM FROM YOUR BAG ");
        System.out.println("\t\t+ DROP <ITEM> END : DROP THE ITEM YOU DON'T NEED IN CURRENT LOCATION");
        System.out.println("\t\t+ QUIT END : LEAVE & END THE GAME");
    }
    private void Axui(){
        System.out.println(" ______   __   __   __  __  ______           \n" +
"/\\  _  \\ /\\ \\ /\\ \\ /\\ \\/\\ \\/\\__  _\\          \n" +
"\\ \\ \\L\\ \\\\ `\\`\\/'/'\\ \\ \\ \\ \\/_/\\ \\/          \n" +
" \\ \\  __ \\`\\/ > <   \\ \\ \\ \\ \\ \\ \\ \\          \n" +
"  \\ \\ \\/\\ \\  \\/'/\\`\\ \\ \\ \\_\\ \\ \\_\\ \\__       \n" +
"   \\ \\_\\ \\_\\ /\\_\\\\ \\_\\\\ \\_____\\/\\_____\\      \n" +
"    \\/_/\\/_/ \\/_/ \\/_/ \\/_____/\\/_____/ ");
    }
    private void Help(String str)
    {   
        str=str.toUpperCase();
        switch(str)
        {
            case "HELP":{
                this.Help();
                break;
            }
            case "HERO":{
                System.out.println("HERO END : MAIN CHARACTER &  CURRENT LOCATION DATA");
                break;
            }
            case "GO":{
                System.out.println("GO <DESTINATION> END : MOVE TO DESTINATION ROOM ");
                break;
            }
            case "LOOK":{
                System.out.println("LOOK <ARG> END : ARG{<ITEM>,<BOT>,<EXIT(DESTINATION)>} DESCRIPTION OF <ARG> ");
                System.out.println("LOOK <WEAPON> END : WEAPON DESCRIPTION ");
                System.out.println("LOOK <Bag> END : YOUR BAG DESCRIPTION ");
                System.out.println("LOOK AROUND END : YOUR CURRENT LOCATION DESCRIPTION ");
                break;
            }
            case "TAKE":{
                System.out.println("TAKE <ITEM> END : TAKE THE ITEM YOU WANT FROM CURRENT LOCATION");
                break;
            }
            case "USE":{
                System.out.println("USE <ITEM> END : USE AN ITEM FROM YOUR BAG");
                break;
            }
            case "DROP":{
                System.out.println("DROP <ITEM> END : DROP THE ITEM YOU DON'T NEED IN CURRENT LOCATION");
                break;
            }
            case "QUIT":{
                System.out.println("\t\t+ QUIT END : LEAVE & END THE GAME");
                break;
            }
            default:{
                System.out.println("xxxxxxxxxxxxxxINVALID COMMANDxxxxxxxxxxxxxx");
            }
        }
    }
}