/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class BombIT {
    
    public BombIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSideEffectDamage method, of class Bomb.
     */
    @Test
    public void testGetSideEffectDamage() {
        System.out.println("getSideEffectDamage");
        int SideEffectDamage=5;
        Bomb instance = new Bomb(SideEffectDamage,10,20,6);
        assertEquals(SideEffectDamage,instance.getSideEffectDamage());
    }

    /**
     * Test of SpecialByHero method, of class Bomb.
     */
    @Test
    public void testSpecialByHero() {
        System.out.println("SpecialByHero");
        Hero hero = new Hero("hero");
        Bomb instance = new Bomb(5,10,20,6);
        hero.initCurentRoom("START_HALL");
        instance.SpecialByHero(hero);
        assertNull(hero.getEquipped());
    }

    /**
     * Test of display method, of class Bomb.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int damage=10;
         int volume= 6;  
         int value= 20;
        int SideEffectDamage=5;
        Bomb instance = new Bomb(SideEffectDamage,damage,value,volume);
       
       assertEquals("Error description","Great damage bomb to your opponents, use it with caution",instance.getDesc());
       assertEquals("Error name's class","Bomb",instance.getClass().getSimpleName());
       assertEquals("Error name ","B-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
       assertEquals(damage,instance.getDamage());
       assertEquals(SideEffectDamage,instance.getSideEffectDamage());
    }

    /**
     * Test of simpleDisplay method, of class Bomb.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        int damage=10;
         int volume= 6;  
         int value= 20;
        int SideEffectDamage=5;
        Bomb instance = new Bomb(SideEffectDamage,damage,value,volume);
       
       
       assertEquals("Error name's class","Bomb",instance.getClass().getSimpleName());
       assertEquals("Error name ","B-0",instance.getName());
       assertEquals(volume,instance.getVolume());
       assertEquals(damage,instance.getDamage());
       assertEquals(SideEffectDamage,instance.getSideEffectDamage());
    }
    
}
