/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class OtherIT {
    
    public OtherIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMessage method, of class Other.
     */
    @Test
    public void testGetMessage() {
        System.out.println("getMessage");
        String mess="you can use this paper to figure out the password";
        Other instance =new Other(mess,"paper",1);
        assertEquals(mess,instance.getMessage());
    }

    /**
     * Test of useByHero method, of class Other.
     */
    @Test
    public void testUseByHero() {
        
    }

    /**
     * Test of takeByHero method, of class Other.
     */
    @Test
    public void testTakeByHero() {
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        
        Other instance=new Other("you can use this paper to figure out the password","paper",1);
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
        hero.initCurentRoom ("DECISION_ROOM");
        instance.takeByHero(hero);
        
    }

    /**
     * Test of display method, of class Other.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
         String msg ="you can use this paper to figure out the password";
         String name="paper";
         int volume= 1;  
         int value= 0;
        Other instance=new Other(msg,name,volume);
       
       assertEquals("Error description","May be useful",instance.getDesc());
       assertEquals("Error name's class","Other",instance.getClass().getSimpleName());
       assertEquals("Error name ",name,instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }

    /**
     * Test of simpleDisplay method, of class Other.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        String msg ="you can use this paper to figure out the password";
         String name="paper";
         int volume= 1;  
         int value= 0;
        Other instance=new Other(msg,name,volume);
       
       
       assertEquals("Error name's class","Other",instance.getClass().getSimpleName());
       assertEquals("Error name ",name,instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }
    
}
