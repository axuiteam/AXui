/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class KeyIT {
    
    public KeyIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of useByHero method, of class Key.
     */
    @Test
    public void testUseByHero() {
        System.out.println("useByHero");
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        Key instance=new Key("key1",10,6);
        assertTrue(hero.hasBag());
        hero.initCurentRoom ("DECISION_ROOM");
        assertTrue(hero.getCurentRoom().hasExit("FIRST_CHALLENGE"));
        instance.useByHero(hero, "key1");
        
    }

    /**
     * Test of takeByHero method, of class Key.
     */
    @Test
    public void testTakeByHero() {
        System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        
        Key instance=new Key("key1",10,6);
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
        hero.initCurentRoom ("DECISION_ROOM");
        instance.takeByHero(hero);
    }

    /**
     * Test of display method, of class Key.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        String name="key1";
         int volume= 3;  
         int value= 50;
        Key instance=new Key(name,value,volume);
       
       assertEquals("Error description","A key, there's somewhere a door that I can open",instance.getDesc());
       assertEquals("Error name's class","Key",instance.getClass().getSimpleName());
       assertEquals("Error name ",name,instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }

    /**
     * Test of simpleDisplay method, of class Key.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        String name="key1";
         int volume= 3;  
         int value= 50;
        Key instance=new Key(name,value,volume);
       
       
       assertEquals("Error name's class","Key",instance.getClass().getSimpleName());
       assertEquals("Error name ",name,instance.getName());
       assertEquals(volume,instance.getVolume());
    }
    
}
