/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class HealIT {
    
    public HealIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of takeByHero method, of class Heal.
     */
    @Test
    public void testTakeByHero() {
        System.out.println("takeByHero");
         Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        
        Heal instance = new Heal(5,10,6);
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
        hero.initCurentRoom ("DECISION_ROOM");
        instance.takeByHero(hero);
    }

    /**
     * Test of useByHero method, of class Heal.
     */
    @Test
    public void testUseByHero() {
        System.out.println("useByHero");
         Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        int Hvalue=5;
        Heal instance = new Heal(Hvalue,10,6);
        int bl=hero.getHp();
        assertTrue(hero.hasBag());
        instance.useByHero(hero, "H-0");
        assertEquals(bl+Hvalue,hero.getHp());
    }

    /**
     * Test of getHealValue method, of class Heal.
     */
    @Test
    public void testGetHealValue() {
        System.out.println("getHealValue");
        int heal=5;
        Heal instance=new Heal(heal,10,6);
        assertEquals(heal,instance.getHealValue());
        
    }

    /**
     * Test of display method, of class Heal.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
          int hv=5;
        int value=10;
        int volume=6;
        Heal instance = new Heal(hv,value,volume);
       
       assertEquals("Error description","A heal,can refill your healthpoint",instance.getDesc());
       assertEquals("Error name's class","Heal",instance.getClass().getSimpleName());
       assertEquals("Error name ","H-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }

    /**
     * Test of simpleDisplay method, of class Heal.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
          int hv=5;
        int value=10;
        int volume=6;
        Heal instance = new Heal(hv,value,volume);
        
        assertEquals("Error name's class","Heal",instance.getClass().getSimpleName());
       assertEquals("Error name ","H-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }
    
}
