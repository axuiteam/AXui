/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class KnifeIT {
    
    public KnifeIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of SpecialByHero method, of class Knife.
     */
    @Test
    public void testSpecialByHero() {
        
    }

    /**
     * Test of display method, of class Knife.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int damage=5;
         int volume= 1;  
         int value= 0;
        Knife instance=new Knife(damage,value,volume);
       
       assertEquals("Error description","A knife, be brave and attack your target",instance.getDesc());
       assertEquals("Error name's class","Knife",instance.getClass().getSimpleName());
       assertEquals("Error name ","KN-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
       assertEquals(damage,instance.getDamage());
    }

    /**
     * Test of simpleDisplay method, of class Knife.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
         int damage=5;
         int volume= 1;  
         int value= 0;
        Knife instance=new Knife(damage,value,volume);
       
       
       assertEquals("Error name's class","Knife",instance.getClass().getSimpleName());
       assertEquals("Error name ","KN-0",instance.getName());
       assertEquals(damage,instance.getDamage());
    }
    
}
