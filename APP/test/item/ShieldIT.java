/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class ShieldIT {
    
    public ShieldIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of takeByHero method, of class Shield.
     */
    @Test
    public void testTakeByHero() {
        System.out.println("takeByHero");
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        
        Shield instance=new Shield(5,10,6); 
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
        hero.initCurentRoom ("DECISION_ROOM");
        instance.takeByHero(hero);
    }

    /**
     * Test of useByHero method, of class Shield.
     */
    @Test
    public void testUseByHero() {
        System.out.println("useByHero");
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        int Svalue=5;
        Shield instance=new Shield(5,10,6);
        int bl=hero.getShield();
        assertTrue(hero.hasBag());
        instance.useByHero(hero, "SH-0");
        assertEquals(bl+Svalue,hero.getShield());
    }

    /**
     * Test of getSP method, of class Shield.
     */
    @Test
    public void testGetSP() {
        System.out.println("getSP");
        int sp=5;
        Shield instance=new Shield(sp,10,6);
        assertEquals(sp,instance.getSP());
    }

    /**
     * Test of display method, of class Shield.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int sp=5;
        int value=10;
        int volume=6;
        Shield instance = new Shield(sp,value,volume);
       
       assertEquals("Error description","A shield,can refill your shieldpoint",instance.getDesc());
       assertEquals("Error name's class","Shield",instance.getClass().getSimpleName());
       assertEquals("Error name ","SH-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }

    /**
     * Test of simpleDisplay method, of class Shield.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        int sp=5;
        int value=10;
        int volume=6;
        Shield instance = new Shield(sp,value,volume);
        assertEquals("Error name's class","Shield",instance.getClass().getSimpleName());
       assertEquals("Error name ","SH-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }
    
}
