/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package item;

import game.Hero;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendahou
 */
public class CoinIT {
    
    public CoinIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of useByHero method, of class Coin.
     */
    @Test
    public void testUseByHero() {
        System.out.println("useByHero");
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        int value=10;
         Coin instance = new Coin(value);
        int bl=hero.getBalance();
        assertTrue(hero.hasBag());
        instance.useByHero(hero, "C-0");
        assertEquals(bl+value,hero.getBalance());
    }

    /**
     * Test of takeByHero method, of class Coin.
     */
    @Test
    public void testTakeByHero() {
       
        Hero hero = new Hero("hero");
        Bag b=new Bag(10,20);
        hero.setBag(b);
        
        Coin instance = new Coin(10); 
        assertTrue(hero.hasBag());
        assertTrue(hero.getBag().addItem(instance));
        hero.initCurentRoom ("DECISION_ROOM");
        instance.takeByHero(hero);
    }

    /**
     * Test of display method, of class Coin.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int value=10;
        int volume=1;
        Coin instance = new Coin(value);
       
       assertEquals("Error description","A coin, increase your balance &  use it to exchange items with other characters",instance.getDesc());
       assertEquals("Error name's class","Coin",instance.getClass().getSimpleName());
       assertEquals("Error name ","C-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
    }

    /**
     * Test of simpleDisplay method, of class Coin.
     */
    @Test
    public void testSimpleDisplay() {
        System.out.println("simpleDisplay");
        System.out.println("SimpleDisplay");
        int value=10;
        int volume=1;
        Coin instance = new Coin(value);
       assertEquals("Error name's class","Coin",instance.getClass().getSimpleName());
       assertEquals("Error name ","C-0",instance.getName());
       assertEquals(value,instance.getValue());
       assertEquals(volume,instance.getVolume());
       
    
    }
    
}
