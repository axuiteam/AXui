/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package item;

import game.Hero;
import place.LockedWithKey;

/**
 *
 * @author bendahou
 */
public class Key extends Item {
    
    private static final String KEY_DESC="A key, there's somewhere a door that I can open"; 
     public Key(String NAME,int VALUE,int VOLUME){
        super(NAME,VALUE,VOLUME,KEY_DESC);
    }

    @Override
    public void useByHero(Hero hero,String Arg){
       if(hero.getCurentRoom().hasExit(Arg)){
           hero.getCurentRoom().getExit(Arg).keyUnLock(this);
           hero.getBag().removeItem(this);
       }else
       {
           System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
       }
       
    }

    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {if(hero.getBag().addItem(this))
                hero.getCurentRoom().removeItem(this.getName());
         }else{
                System.out.println("you don't have a bag");
            }
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Volume : "+this.getVolume());
    }
    
}
