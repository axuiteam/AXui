
package item;

import game.Hero;


public class Other extends Item  {
    private static final int DEFAULT_VALUE=0;
    private final String MESSAGE ;
    private static final String OTHER_DESC="May be useful"; 
    
    
    public Other(String message,String NAME,int VOLUME){
        super(NAME,Other.DEFAULT_VALUE,VOLUME,OTHER_DESC);
        this.MESSAGE=message;
    }
    
    public String getMessage()
    {
        return this.MESSAGE;
    }
    @Override
    public void useByHero(Hero hero,String Arg){
        System.out.println("\t\t_____________________________________________");
        System.out.println("\t\t| Name : "+this.getName());
        System.out.println("\t\t\t| Message : "+this.getMessage());
        System.out.println("\t\t_____________________________________________");
    }

    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {if(hero.getBag().addItem(this))
                hero.getCurentRoom().removeItem(this.getName());
         }else{
                System.out.println("you don't have a bag");
            }
         
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Message : "+this.MESSAGE);
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Volume : "+this.getVolume());
    }
    
    
}
