
package item;

import game.Hero;


public class Bomb extends Weapon{
    private static final String BOMB_DESC="Great damage bomb to your opponents, use it with caution"; 
    private static int namIndx=0;
    private final int SIDE_EFFECT_DAMAGE;
    
    public Bomb(int SidEffectDamage ,int damage,int VALUE,int VOLUME){
        super(damage,"B-"+Bomb.namIndx,VALUE,VOLUME,Bomb.BOMB_DESC);
        this.SIDE_EFFECT_DAMAGE=SidEffectDamage;
        Bomb.namIndx++;
        
    }
    
    public int getSideEffectDamage(){
        return this.SIDE_EFFECT_DAMAGE;
    }

    @Override
    public void SpecialByHero(Hero hero) {
        System.out.println("BOOOOOOOOOOOMB");
        hero.getCurentRoom().sideEffects(this.getSideEffectDamage());
        hero.setEquipped(null);

    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Damage : "+this.getDamage());
        System.out.println("\t\t Side Effect Damage : "+this.getSideEffectDamage());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Damage : "+this.getDamage());
        System.out.println("\t\t Side Effect Damage : "+this.getSideEffectDamage());
    }
}
