
package item;

import game.Hero;


public interface Usable {
    
    public abstract void useByHero(Hero hero,String Arg);
}
