
package item;

import game.Hero;


public class Heal extends Item{
    
    private final int HEALVALUE;
    private static int namIndx=0;
    private static final String HEAL_DESC="A heal,can refill your healthpoint";
    public Heal(int HealValue,int VALUE,int VOLUME){
        super("H-"+Heal.namIndx,VALUE,VOLUME,HEAL_DESC);
        this.HEALVALUE=HealValue;
        Heal.namIndx++;
    }

    @Override
    public void takeByHero(Hero hero) {
         if(hero.hasBag())
         {if(hero.getBag().addItem(this))
                hero.getCurentRoom().removeItem(this.getName());
         }else{
                System.out.println("you don't have a bag");
            }
         
    }

    @Override
    public void useByHero(Hero hero,String Arg) {
        hero.increaseHp(this.getHealValue());
        hero.getBag().removeItem(this);
        System.out.println("Hp ++");
        
    }
    public int getHealValue()
    {
        return this.HEALVALUE;
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Heal Value : "+this.getHealValue());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Heal Value : "+this.getHealValue());
    }
    
}
