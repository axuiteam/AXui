
package item;

import game.Hero;


public class Knife extends Weapon{
    private static int namIndx=0;
    private static final String KNIFE_DESC="A knife, be brave and attack your target"; 
    
    public Knife(int damage,int VALUE,int VOLUME){
        super(damage,"KN-"+Knife.namIndx,VALUE,VOLUME,Knife.KNIFE_DESC);
        Knife.namIndx++;
    }

    @Override
    public void SpecialByHero(Hero hero) {
        System.out.println("/////////////////");
           
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Value : "+this.getValue());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Damage : "+this.getDamage());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Volume : "+this.getVolume());
        System.out.println("\t\t Damage : "+this.getDamage());
    }
    
}
