package game;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;




public class Game {

    
    private Hero hero;
    
    public Game()
    {
        this.Axui();
        System.out.println(" ---------------------------------------------------- ");
        System.out.println("|xxxxx                AXui                      xxxxx|");
        System.out.println("");
        System.out.println("\t\t\t1-new Game");
        System.out.println("\t\t\t2-Records");
        System.out.println("\t\t\t3-About US");
        System.out.println("\t\t\t4-Quit");
        System.out.println("|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx|");
        System.out.println(" ---------------------------------------------------- ");
        System.out.println("");
      
        Scanner sc = new Scanner(System.in);
        int n;
        boolean b=true;
        while(b){
                System.out.print(">>");
                
                try{
                n=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    n=0;
                }
                System.out.println("");   
                switch(n)
                {   
                    case 1:{System.out.println("");
                        this.startGame();
                        b=false;
                        break;
                    }
                    case 2:{
                        this.records();
                        b=true;
                        break;
                    }
                    case 3:{
                        this.aboutUs();
                        b=true;
                        break;
                    }
                    case 4:{
                        this.quit();
                        break;
                    }
                    default :{
                        System.out.println("XxX INVALID XxX");
                        b=true;
                        break;
                    }
                }

    }
    }
    public void startGame()
    {
        long TimeStart=0;
        this.Story();
        System.out.println("enter your Name :");
        String name ="";
        Scanner sc = new Scanner(System.in);
        while(name.isBlank()){
                System.out.print(">>");
                name=sc.nextLine();
                name=name.trim().toUpperCase();
        }
        this.hero=new Hero(name);
        this.hero.initCurentRoom("START_HALL");
        String com;
        while(this.hero.NotYet())
        {
            this.hero.getCurentRoom().monstersAttackHero();
            TimeStart=System.currentTimeMillis();
            System.out.println("");
            System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
            System.out.print(">>");
            com = sc.nextLine();
            com=com.trim();
            if(!Game.validComannd(com)){
                System.out.println("xxxxxxxxxxxxxxINVALID COMMANDxxxxxxxxxxxxxx");
                
            }else{
                 
                 String[] Args=Game.getArgs(com);
                 Command c= Command.valueOf(Args[0]);
                 switch(c){
                     case QUIT:{
                         this.quit();
                         break;
                     }
                     case HERO: {
                         this.hero.ShowHero();
                         
                         break;
                     }
                     case GO :{
                         if(this.hero.getCurentRoom().hasMonsters())
                         {
                             System.out.println(">>>>>you can't run away");
                             break;
                         }
                         if ((Args.length==3))
                         {
                             this.hero.go(Args[1]);
                             if(this.hero.getCurentRoom().hasMonsters())
                                 System.out.println("monsters !!!!");
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case LOOK :{
                         if ((Args.length==3))
                         {
                             this.hero.look(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case HELP :{
                         if(Args.length==2 || Args.length==3){
                            if ((Args.length==2))
                            {
                                this.Help();
                            }
                            if ((Args.length==3))
                            {
                                this.Help(Args[1]);
                            }
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case TAKE:
                     {
                         if ((Args.length==3))
                         {
                             this.hero.takeItem(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case DROP:
                     {
                         if ((Args.length==3))
                         {
                             this.hero.dropItem(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case USE:
                     {
                         if ((Args.length>=3 && Args.length<5))
                         {
                             
                             this.hero.useItem(Args);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case SPEAK:
                     {
                         if ((Args.length==3))
                         {
                             this.hero.speakToBot(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     case ATTACK:
                     {
                         if ((Args.length==3))
                         {
                             this.hero.attackBot(Args[1]);
                         }else{
                         System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                         }
                         break;
                     }
                     
                     
                     
                 }
            }
            
            System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
            System.out.println("");
        
        }
        this.END(TimeStart);
    
    }
    public void records(){
      String file=".Axuirecords.txt";   
      try {
       
      File myObj = new File(file);
      if(myObj.createNewFile()){
          FileWriter myWriter = new FileWriter(file);
          myWriter.write("NAME\tSCORE\tTIME(s)");
          myWriter.close();

          
      }
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
              String data = myReader.nextLine();
              System.out.println(data);
            }
        myReader.close();
      
    }catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
        private void addRecords(String name,int score,Long time)
    {
        
    try {
      String file=".Axuirecords.txt";  
      File myObj = new File(file);
      if(myObj.createNewFile()){
        FileWriter myWriter = new FileWriter(file);
        myWriter.write("NAME\tSCORE\tTIME(s)");
        myWriter.close();  
        Files.write(Paths.get(file),("\n"+name+"\t"+score+"\t"+time).getBytes(), StandardOpenOption.APPEND);
      }else{
      Files.write(Paths.get(file),("\n"+name+"\t"+score+"\t"+time).getBytes(), StandardOpenOption.APPEND);
      }
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
 }
    public void quit(){
        System.exit(0);
    }
    private static boolean validComannd(String str){
        str= str.toUpperCase();
        String [] coms = str.split(" ",0);
        if(coms.length <2 || !coms[coms.length-1].equals("END"))
        return false;
        try{
            Command c = Command.valueOf(coms[0]);
        }catch(Exception e){
            return false;
        }
        return true;
    }
    
    private static String[] getArgs(String str){
        str= str.toUpperCase();
        str.replace("END", "");
        String [] coms = str.split(" ",0);
        return coms;
    }
    private void Story() {
        String str = "I hope you’re ready to live such an exciting experience.\n"
                + "Enter strange and unexplored rooms full of creatures, dangers,\n"
                + "and surprises. Your only companion to face hardships is your magic bag,\n"
                + "and your main objective is to win the game by going through all the exits to arrive to the final room.";
        
        System.out.println(str);
        System.out.print("\npress entrer >");
        try
        {
            System.in.read();
            
        }  
        catch(Exception e)
        {
        
        }
        System.out.println("");
    }
    private void aboutUs()
    {
        System.out.println("\t\tthe project will be published on November 15, 2021\n\t\t" +
"    GitLab link:https://gitlab.com/axuiteam/AXui");
    
    }
    private void END(long T){
        if((this.hero.getCurentRoom().getName()).equals("THE_END")){
            T-=System.currentTimeMillis();
            this.Win();
            int score= (this.hero.getHp()*2)+this.hero.getBalance()+(this.hero.getRespect()*3);
            this.addRecords(this.hero.getName(),score ,-1*T/1000 );
            this.records();
        }else{
            this.GameOver();
        }
    }
    
    private void Help(){
        System.out.println("\t\t\t-x COMMANDS LIST x-");
        System.out.println("\t\t- HELP END : COMMANDS LIST ");
        System.out.println("\t\t\t+ HELP <COMMAND> END : HOW TO USE COMMAND ");
        System.out.println("\t\t- HERO END : MAIN CHARACTER &  CURRENT LOCATION DATA{MONSTERS}");
        System.out.println("\t\t- GO <EXIT(DESTINATION)> END : MOVE TO DESTINATION ROOM ");
        System.out.println("\t\t- LOOK <ARG> END : ARG{<ITEM(NAME)>,<BOT(NAME)>,<EXIT(DESTINATION)>} DESCRIPTION OF <ARG> ");
        System.out.println("\t\t\t+ LOOK <WEAPON> END :YOUR WEAPON DESCRIPTION ");
        System.out.println("\t\t\t+ LOOK <BAG> END : YOUR BAG DESCRIPTION ");
        System.out.println("\t\t\t+ LOOK AROUND END : YOUR CURRENT LOCATION DATA{EXIT,CARACTERS,ITEMS,MONSTERS}");
        System.out.println("\t\t- TAKE <ITEM(NAME)> END : TAKE THE ITEM YOU WANT FROM CURRENT LOCATION");
        System.out.println("\t\t- USE <ITEM(NAME)> END : USE AN ITEM FROM YOUR BAG ");
        System.out.println("\t\t- DROP <ITEM(NAME)> END : DROP THE ITEM YOU DON'T NEED IN CURRENT LOCATION");
        System.out.println("\t\t\t+ DROP <ALL> END : DROP ALL ITEMS FROM BAG");
        System.out.println("\t\t\t+ DROP <BAG> END : DROP YOUR BAG ,ALL ITEMS INSIDE THE BAG WILL BE DROPED IN THE ROOM");
        System.out.println("\t\t- SPEAK <MONSTER OR CHARACTER(NAME)> END : SPEAK TO MONSTER OR CHARACTER");
        System.out.println("\t\t- ATTACK <MONSTER OR CHARACTER(NAME)> END : ATTACK TO MONSTER OR CHARACTER");
        System.out.println("\t\t- QUIT END : LEAVE & END THE GAME");
    }
    private void Axui(){
        System.out.println(" ______   __   __   __  __  ______           \n" +
"/\\  _  \\ /\\ \\ /\\ \\ /\\ \\/\\ \\/\\__  _\\          \n" +
"\\ \\ \\L\\ \\\\ `\\`\\/'/'\\ \\ \\ \\ \\/_/\\ \\/          \n" +
" \\ \\  __ \\`\\/ > <   \\ \\ \\ \\ \\ \\ \\ \\          \n" +
"  \\ \\ \\/\\ \\  \\/'/\\`\\ \\ \\ \\_\\ \\ \\_\\ \\__       \n" +
"   \\ \\_\\ \\_\\ /\\_\\\\ \\_\\\\ \\_____\\/\\_____\\      \n" +
"    \\/_/\\/_/ \\/_/ \\/_/ \\/_____/\\/_____/ ");
    }
    private void GameOver(){
        System.out.println("  _____          __  __ ______    ______      ________ _____  \n" +
"  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ \n" +
" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |\n" +
" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / \n" +
" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ \n" +
"  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\");
    }
    private void Win(){
        System.out.println("                              _       \n" +
"                             (_)      \n" +
" _   _  ___  _   _  __      ___ _ __  \n" +
"| | | |/ _ \\| | | | \\ \\ /\\ / / | '_ \\ \n" +
"| |_| | (_) | |_| |  \\ V  V /| | | | |\n" +
" \\__, |\\___/ \\__,_|   \\_/\\_/ |_|_| |_|\n" +
"  __/ |                               \n" +
" |___/                                ");
        System.out.println("");
    }
    private void Help(String str)
    {   
        str=str.toUpperCase();
        switch(str)
        {
            case "HELP":{
                this.Help();
                break;
            }
            case "HERO":{
                System.out.println("HERO END : MAIN CHARACTER &  CURRENT LOCATION DATA");
                break;
            }
            case "GO":{
                System.out.println("GO <DESTINATION> END : MOVE TO DESTINATION ROOM ");
                break;
            }
            case "LOOK":{
                System.out.println("LOOK <ARG> END : ARG{<ITEM>,<BOT>,<EXIT(DESTINATION)>} DESCRIPTION OF <ARG> ");
                System.out.println("\t\t+LOOK <WEAPON> END : WEAPON DESCRIPTION ");
                System.out.println("\t\t+LOOK <Bag> END : YOUR BAG DESCRIPTION ");
                System.out.println("\t\t+LOOK AROUND END : YOUR CURRENT LOCATION DESCRIPTION ");
                break;
            }
            case "TAKE":{
                System.out.println("TAKE <ITEM> END : TAKE THE ITEM YOU WANT FROM CURRENT LOCATION");
                break;
            }
            case "USE":{
                System.out.println("USE <ITEM> END : USE AN ITEM FROM YOUR BAG");
                break;
            }
            case "DROP":{
                System.out.println("DROP <ITEM> END : DROP THE ITEM YOU DON'T NEED IN CURRENT LOCATION");
                System.out.println("\t\t+ DROP <ALL> END : DROP ALL ITEMS FROM BAG");
                System.out.println("\t\t+ DROP <BAG> END : DROP <BAG> END : DROP YOUR BAG ,ALL ITEMS INSIDE THE BAG WILL BE DROPED IN THE ROOM");
                break;
            }
            case "ATTACK":{
               System.out.println("SPEAK <MONSTER OR CHARACTER(NAME)> END : SPEAK TO MONSTER OR CHARACTER");
               System.out.println("ATTACK <MONSTER OR CHARACTER(NAME)> END : ATTACK TO MONSTER OR CHARACTER");
                break;
            }
            case "SPEAK":{
                System.out.println("SPEAK <MONSTER OR CHARACTER(NAME)> END : SPEAK TO MONSTER OR CHARACTER");
                break;
            }
            case "QUIT":{
                System.out.println("QUIT END : LEAVE & END THE GAME");
                break;
            }
            default:{
                System.out.println("xxxxxxxxxxxxxxINVALID COMMANDxxxxxxxxxxxxxx");
            }
        }
    }
}