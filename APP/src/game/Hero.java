package game;

import item.Bag;
import item.Weapon;
import place.Place;




public class Hero {

	private Weapon equipped;
	private Bag bag;
	private Place curentRoom;
	private final String NAME;
	private int hp;
	private int respect;
	private int balance;
	private int shield;
	private static final int MAX_HP = 3000;
	private static final int MAX_SHIELD = 5000;
	private static final int DEFAULT_HP = 2000;
	private static final int DEFAULT_SHIELD = 4000;
	private static final int DEFAULT_RESPECT = 10;
	private static final int DEFAULT_DAMAGE = 10;
        private static final int DEFAULT_BALANCE = 10;
	private int damage;

	/**
	 * 
	 * @param name
	 */
	public Hero(String name) {
		// TODO - implement Hero.Hero
		this.NAME=name;
                this.hp=Hero.DEFAULT_HP;
                this.balance=Hero.DEFAULT_BALANCE;
                this.damage=Hero.DEFAULT_DAMAGE;
                this.respect=Hero.DEFAULT_RESPECT;
                this.shield=Hero.DEFAULT_SHIELD;
                
	}

	/**
	 * 
	 * @param price
	 */
	public boolean hasBalanceFor(int price) {
		if(this.balance>=price)
                    return true;
                return false;
	}

	public int getHp() {
		return this.hp;
	}
        public int getBalance() {
		return this.balance;
	}
        public void respectUp(int a){
            this.respect+=a;
        }
        public void respectDown(int a){
            this.respect-=a;
        }
	public int getRespect() {
		return this.respect;
	}

	public String getName() {
		// TODO - implement Hero.getName
		return this.NAME;
	}
        public Place getCurentRoom(){
            return this.curentRoom;
        }

	/**
	 * 
	 * @param dec
	 */
	public void decreaseHp_Shield(int dec) {
		if(this.shield <= dec)
                {
                    dec-=this.shield;
                    this.shield=0;
                    this.hp-=dec;
                }else{
                    this.shield-=dec;
                }
		
	}

	/**
	 * 
	 * @param bal
	 */
	public void decreaseBalance(int bal) {
		this.balance-=bal;
	}
        public void increaseBalance(int bal){
            this.balance+=bal;
        }
        public void increaseHp(int hp){
            if(this.hp+hp  >=Hero.MAX_HP)
            {
                this.hp=Hero.MAX_HP;
            }else{
                this.hp+=hp;
            }
        }
         public void increaseShield(int sp){
            if(this.shield+sp  >=Hero.MAX_SHIELD)
            {
                this.shield=Hero.MAX_SHIELD;
            }else{
                this.shield+=sp;
            }
        }

	public int getShield() {
		return this.shield;
	}
        
	public void attackBot(String bot) {
		if(this.getCurentRoom().hasMonster(bot))
                {
                    this.getCurentRoom().getMonster(bot).attackedByHero(this);
                    if(this.equipped!=null)
                        this.equipped.SpecialByHero(this);
                }else{
                    if(this.getCurentRoom().hasCharctero(bot))
                    {
                        this.getCurentRoom().getCharatero(bot).attackedByHero(this);
                        if(this.equipped!=null)
                        this.equipped.SpecialByHero(this);
                    }else
                        System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                }
		
	}

	/**
	 * 
	 * @param item
	 */
	public void useItem(String[] items) {
                
                
		if(this.hasBag()&& this.bag.hasItem(items[1]))
                {
                    if(items.length==3)
                    this.bag.getItem(items[1]).useByHero(this,"");
                    if(items.length==4)
                        this.bag.getItem(items[1]).useByHero(this,items[2]);
                }else
                {
                    System.out.println("item not found");
                }
	}

	/**
	 * 
	 * @param exit
	 */
	public void go(String exit) {
		if(this.curentRoom.hasExit(exit))
                {
                    if(this.curentRoom.getExit(exit).open()){
                       
                        try {
                            System.out.print(this.curentRoom.getName()+"--");
                            Thread.sleep(300);
                            System.out.print(" -- ");
                            Thread.sleep(300);
                            System.out.print(" -> ");
                            Thread.sleep(300);
                        } catch (InterruptedException ex) {
                            
                        }
                        
                        
                        this.curentRoom.setHero(null);
                        this.curentRoom=GameMap.getPlace(this.curentRoom.getExit(exit).getDestination());
                        this.curentRoom.setHero(this);
                        System.out.println(this.curentRoom.getName());
                        if(this.curentRoom.hasMonsters())
                        {
                            System.out.println("watch Monsters !");
                            this.getCurentRoom().showMonsters();
                        }
                    }else{
                        System.out.println("oops failed to open the exit");
                    }
                }else {
                    System.out.println("oops your destination does not exist");
                }     
        }

	/**
	 * 
	 * @param bot
	 */
	public void speakToBot(String bot) {
		if(this.getCurentRoom().hasMonster(bot))
                {
                    this.getCurentRoom().getMonster(bot).speackWithHero(this);
                }else{
                    if(this.getCurentRoom().hasCharctero(bot))
                    {
                        this.getCurentRoom().getCharatero(bot).speackWithHero(this);
                    }else
                        System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");
                }
                
	}

	/**
	 * 
	 * @param observ
	 */
	public void look(String observ) {
		if(observ.equals("AROUND"))
                {
                    this.getCurentRoom().display();
                    if(this.bag != null)
                        {
                            this.bag.simpleDisplay();
                        }
                    
                }else
                {
                    if(observ.equals("WEAPON"))
                    {
                        if(this.equipped != null)
                        {
                            this.equipped.display();
                        }else{
                            System.out.println("use <Weapon>? ");
                        }
                    
                    }else{
                        if(observ.equals("BAG"))
                        {
                            if(this.bag != null)
                            {
                                this.bag.display();
                                return;
                            }

                        }else
                            if(this.getCurentRoom().hasItem(observ)){
                                this.getCurentRoom().showItem(observ);
                                return;
                            }
                            
                            if(this.getCurentRoom().hasMonster(observ)){
                                this.getCurentRoom().showMonster(observ);
                                return;
                            }
                            if(this.getCurentRoom().hasCharctero(observ)){
                                this.getCurentRoom().showCharctero(observ);
                                return;
                            }
                            if(this.curentRoom.hasExit(observ))
                            {
                                this.getCurentRoom().getExit(observ).display();
                                return;    
                            }
                            if(this.bag!=null){
                                if(this.bag.hasItem(observ))
                                {
                                    this.bag.getItem(observ).display();
                                    return ;
                                }
                            }
                            System.out.println("xxxxxxxxxxxxxxINVALID ARGSxxxxxxxxxxxxxx");

                    }
                 }
        }

	public int getDamage() {
		return this.damage;
	}
        private void setDammage()
        {
            if(this.equipped!=null)
                this.damage=this.equipped.getDamage();
            else
                this.damage=Hero.DEFAULT_DAMAGE;
        }

	/**
	 * 
	 * @param name
	 */
	public void takeItem(String name) {
		if(this.getCurentRoom().hasItem(name)){
                            this.getCurentRoom().getItem(name).takeByHero(this);
                            
                }else{
                    System.out.println("item not founded"); 
                }
	}

	public void dropItem(String i) {
            if(this.equipped!=null && this.equipped.getName().equals(i))
            {    
                 this.curentRoom.addItem(this.equipped);
                 this.setEquipped(null);
                 System.out.println("Weapon  x");
                 this.setDammage();
            }else
            {
                if(i.equals("BAG")){
                    this.bag.dropAllItem(curentRoom);
                    this.curentRoom.addItem(this.bag);
                    System.out.println("Bag x");
                    this.bag=null;
                }
                else{
                    if(i.equals("ALL")){
                     this.bag.dropAllItem(this.curentRoom);
                     System.out.println("Item *-");
                    }else{
                        if(this.bag.hasItem(i))
                            {
                                this.bag.dropItem(i, curentRoom);
                                System.out.println("Item -");
                            }
                            else{
                                System.out.println("item not founded"); 
                            }
                    }

                }
            }
	}

	private boolean isAlive() {
		// TODO - implement Hero.isAlive
		if(this.hp>0)
                    return true;
                
                return false;
                                    
	}

        
        public boolean NotYet(){
            return this.isAlive() && !this.curentRoom.getName().equals("THE_END");
        }
        public void ShowHero(){
            System.out.println("\t\t\t\t-"+this.NAME+"-");
            System.out.println("\t\t\tHP  :"+this.hp);
            System.out.println("\t\t\tSHIELD  :"+this.shield);
            System.out.println("\t\t\tBALANCE  :"+this.balance+" B");
            System.out.println("\t\t\tDAMAGE  :"+this.damage);
            System.out.println("[CURRENT PLACE]");
            this.getCurentRoom().simpleDisplay();
            
            if(this.bag != null)
            {
                System.out.println("[BAG]");
                
                this.bag.simpleDisplay();
            }
            if(this.equipped != null)
            {
                System.out.println("[Weapon]");
                
                this.equipped.simpleDisplay();
            }
            
            
        }
        public void initCurentRoom (String place){
            this.curentRoom=GameMap.getPlace(place);
            this.curentRoom.setHero(this);
            if(this.curentRoom.hasMonsters())
                        {
                            System.out.println("\nwatch Monsters !!!\n");
                            this.getCurentRoom().showMonsters();
                        }
        }
        
        public Bag getBag(){
            return bag;
        }
        
        public void setBag(Bag bag){
            this.bag=bag;
        }
        public void setEquipped(Weapon wpn){
            if(wpn!=null){
            this.equipped=wpn;
            this.setDammage();
            }else
            {
                this.equipped=null;
                this.setDammage();
            }
        }
        public Weapon getEquipped(){
            return this.equipped;
            
        }
        public boolean hasBag()
        {
            if(this.bag != null)
                return true;
            return false;
        }
        public boolean hasWeapon()
        {
            if(this.equipped != null)
                return true;
            return false;
        }
        
        
}