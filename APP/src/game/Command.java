package game;

public enum Command {
	DROP,
	GO,
	HELP,
	LOOK,
	TAKE,
	USE,
	ATTACK,
	SPEAK,
        HERO,
	QUIT;       
}