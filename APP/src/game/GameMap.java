package game;
import bot.Charactero;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import place.*;
import item.*;

import bot.*;
public class GameMap {

	public static Map<String,Place> rooms;
        
        static{
            Key keytemp;
            LockedWithPassword doortemp;
            GameMap.rooms = new HashMap<>();
            List<Exit> exits = new ArrayList<>();
            List<Item> items = new ArrayList<>();
            List<Monster> mons = new ArrayList<>();
            List<Charactero> caras = new ArrayList<>();
            
            items.add(new Coin(20));
            items.add(new Coin(40));
            items.add(new Bag(150,280));
            exits.add(new OpenExit("DOOR-TO-GEAR_ROOM","GEAR_ROOM"));
            exits.add(new OpenExit("DOOR-TO-FIRST_CHALLENGE","FIRST_CHALLENGE"));
            Place temp = new Place("START_HALL",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Knife(180,400,90));
            items.add(new Heal(300,100,10));
            items.add(new Heal(60,10,1));
            items.add(new Coin(80));
            items.add(new Coin(80));
            items.add(new Shield(1000,300,40));
            items.add(new Other("take all you can","BE_READY",1));
            items.add(new Bomb(80,700,1200,80));
            items.add(new Bomb(35,400,600,50));
            items.add(new Bomb(35,400,600,50));
            exits.add(new OpenExit("DOOR-TO-START_HALL","START_HALL"));
            temp = new Place("GEAR_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            doortemp=new LockedWithPassword("DOOR-TO-DECISION_ROOM","DECISION_ROOM");
            exits.add(new OpenExit("DOOR-TO-START_HALL","START_HALL"));
            exits.add(doortemp);
            keytemp =new Key("KEY-DOOR-TO-GREEN_HOUSE",2,2);
            items.add(keytemp);
            mons.add(new Monster("JK6",215,100,items));
            exits.add(new LockedWithKey("DOOR-TO-GREEN_HOUSE","GREEN_HOUSE",keytemp));
            items = new ArrayList<>();
            items.add(new Coin(200));
            items.add(new Coin(20));
            temp = new Place("FIRST_CHALLENGE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Bomb(80,700,1200,80));
            caras.add(new NeedHelp("DSG",100,100,items));
            items = new ArrayList<>();
            items.add(doortemp.getPassword());
            items.add(new Bomb(35,400,600,50));
            items.add(new Knife(400,1000,100));
            items.add(new Coin(200));
            items.add(new Coin(20));
            exits.add(new OpenExit("DOOR-TO-FIRST_CHALLENGE","FIRST_CHALLENGE"));
            temp = new Place("GREEN_HOUSE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Shield(1000,300,40));
            items.add(new Knife(400,1000,100));
            items.add(new Heal(1000,800,80));
            caras.add(new Trader("TTY",100,items));
            caras.add(new Helper("HHS",100,"DECIDE RIGHT OR LEFT ,YOU CAN'T COME BACK HERE AGAIN"));
            exits.add(new OpenExit("DOOR-TO-FIRST_CHALLENGE","FIRST_CHALLENGE"));
            exits.add(new OpenExit("DOOR-TO-LEFT_SIDE","LEFT_SIDE"));
            exits.add(new OpenExit("DOOR-TO-RIGHT_SIDE","RIGHT_SIDE"));
            items = new ArrayList<>();
            items.add(new Bag(300,500));
            temp = new Place("DECISION_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            

            
            
            
            //leftside
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Heal(650,300,40));
            mons.add(new Monster("SXZ",215,100,items));
            exits.add(new OpenExit("DOOR-TO-L_BALCONY","L_BALCONY"));
            items = new ArrayList<>();
            items.add(new Shield(1000,300,40));
            temp = new Place("LEFT_SIDE",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            doortemp=new LockedWithPassword("DOOR-TO-ADMIN","ADMIN");
            items.add(new Bomb(80,700,1500,80));
            items.add(new Heal(1000,800,80));
            caras.add(new Trader("TQKL",100,items));
            items = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-LEFT_SIDE","LEFT_SIDE"));
            items.add(doortemp.getPassword());
            mons.add(new Monster("SXJ",450,150,items));
            exits.add(doortemp);
            items = new ArrayList<>();
            items.add(new Shield(1000,300,40));
            temp = new Place("L_BALCONY",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            caras.add(new Helper("GGQ",150,"SUISCIDE ROOM HAS 2 POWERFULLMONSTER"));
            exits.add(new OpenExit("DOOR-TO-L_BALCONY","L_BALCONY"));
            exits.add(new OpenExit("DOOR-TO-SUICIDE_ROOM","SUICIDE_ROOM"));
            exits.add(new OpenExit("DOOR-TO-COWARDS","COWARDS"));
            temp = new Place("ADMIN",exits,items,mons,caras);
            items.add(new Shield(1000,300,40));
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            caras.add(new NeedHelp("QA",100,30,items));
            items = new ArrayList<>();
            caras.add(new NeedHelp("QB",100,100,items));
            items = new ArrayList<>();
            caras.add(new NeedHelp("c",100,50,items));
            items = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-LAIRS_ROOM","LAIRS_ROOM"));
            exits.add(new OpenExit("DOOR-TO-ADMIN","ADMIN"));
            temp = new Place("COWARDS",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Coin(600));
            mons.add(new Monster("QQ1",1200,200,items));
            items = new ArrayList<>();
            items.add(new Coin(600));
            items.add(new Bomb(200,2000,3000,100));
            mons.add(new Monster("QQ2",1200,200,items));
            exits.add(new OpenExit("DOOR-TO-ADMIN","ADMIN"));
            exits.add(new OpenExit("DOOR-TO-RECOVER","RECOVER"));
            items = new ArrayList<>();
            temp = new Place("SUICIDE_ROOM",exits,items,mons,caras);
            items.add(new Shield(1000,300,40));
            GameMap.rooms.put(temp.getName(),temp);
            
            
            
            
            //RIGHTSIDE
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Heal(800,800,40));
            mons.add(new Monster("Z-MON",215,100,items));
            exits.add(new OpenExit("DOOR-TO-R_BALCONY","R_BALCONY"));
            items = new ArrayList<>();
            temp = new Place("RIGHT_SIDE",exits,items,mons,caras);
            items.add(new Shield(1000,300,40));
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            caras.add(new Helper("JTR",150,"USE BOMBS TO KILL PIGS"));
            items.add(new Bomb(80,700,1500,80));
            items.add(new Heal(1000,1000,80));
            caras.add(new Trader("TQKR",100,items));
            items = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-RIGHT_SIDE","RIGHT_SIDE"));
            exits.add(new OpenExit("DOOR-TO-PIGS_ROOM","PIGS_ROOM"));
            temp = new Place("R_BALCONY",exits,items,mons,caras);
            items.add(new Shield(1000,300,40));
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            items.add(new Coin(10));
            mons.add(new Pig("P-0",items));
            mons.add(new Pig("P-2",items));
            mons.add(new Pig("P-3",items));
            mons.add(new Pig("P-4",items));
            mons.add(new Pig("P-5",items));
            mons.add(new Pig("P-6",items));
            mons.add(new Pig("P-7",items));
            mons.add(new Pig("P-8",items));
            items.add(new Coin(300));
            items.add(new Heal(300,25,50));
            mons.add(new Pig("P-1",items));
            caras = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-LAIRS_ROOM","LAIRS_ROOM"));
            temp = new Place("PIGS_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            caras.add(new Helper("INDEX",100,"ONLY ME AND ANOTHER ONE IN THE ROOM HWO TELL THE TRUTH BE SMART BE CAREFUL"));
            caras.add(new Unknown("A",150,"C telling the truth,Y safe",true,items));
            caras.add(new Unknown("B",150,"Y SAFE",true,items));
            caras.add(new Unknown("C",150,"D lie,W not safe",true,items));
            caras.add(new Unknown("D",150,"B & A liars,W safe",false,items));
            exits.add(new OpenExit("DOOR-TO-X","X"));
            exits.add(new OpenExit("DOOR-TO-Y","Y"));
            exits.add(new OpenExit("DOOR-TO-Z","Z"));
            exits.add(new OpenExit("DOOR-TO-W","W"));
            temp = new Place("LAIRS_ROOM",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            //ROOMS X-Y-W-Z
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Coin(1000));
            caras.add(new Helper("GG",100,"SMART OR LUCKY ,ANYWAY GOOD LUCK"));
            exits.add(new OpenExit("DOOR-TO-JUDGMENT","JUDGMENT"));
            temp = new Place("W",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            items.add(new Heal(300,100,30));
            items.add(new Coin(500));
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            mons.add(new Monster("Z-MON",215,100,items));
            items = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            temp = new Place("Z",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            items.add((new Shield(1000,200,30)));
            items.add(new Coin(500));
            mons = new ArrayList<>();
            mons.add(new Monster("X-MON",215,100,items));
            caras = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            temp = new Place("X",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            exits = new ArrayList<>();
            items = new ArrayList<>();
            items.add(new Heal(250,100,30));
            items.add(new Coin(500));
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            mons.add(new Monster("Y-MON",215,100,items));
            items = new ArrayList<>();
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            temp = new Place("Y",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            //RECOVER
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            items.add(new Coin(9000));
            caras.add(new NeedHelp("DID",100,8000,items));
            items = new ArrayList<>();
            items.add(new Heal(30,0,1));
            items.add(new Heal(30,0,1));
            items.add(new Heal(30,0,1));
            items.add(new Heal(80,10,5));
            items.add(new Heal(150,15,10));
            caras.add(new Doctor("FIS",100,items));
            exits.add(new OpenExit("DOOR-TO-SUICIDE_ROOM","SUICIDE_ROOM"));
            exits.add(new OpenExit("DOOR-TO-JUDGMENT","JUDGMENT"));
            items = new ArrayList<>();
            temp = new Place("RECOVER",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            
            //JUDGEMENT
            keytemp=new Key("KEY-DOOR-TO-THE_END",100000,5);
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            caras.add(new NeedHelp("TIK",100,3500,items));
            items.add(keytemp);
            caras.add(new Judge("JUDGE",items));
            items = new ArrayList<>();
            items.add(new Shield(3000,2000,500));
            items.add(new Knife(500,2000,160));
            items.add(new Heal(1000,1000,80));
            caras.add(new Trader("TOK",100,items));
            exits.add(new OpenExit("DOOR-TO-HELL","HELL"));
            exits.add(new LockedWithKey("DOOR-TO-THE_END","THE_END",keytemp));
            items = new ArrayList<>();
            temp = new Place("JUDGMENT",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            //THE HELL
            doortemp=new LockedWithPassword("DOOR-TO-THE_END","THE_END");
            exits = new ArrayList<>();
            items = new ArrayList<>();
            items.add(doortemp.getPassword());
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            mons.add(new Monster("ORI",4000,"THE FINAL MONS",400,items));
            exits.add(new LockedWithPassword("DOOR-TO-THE_END","THE_END"));
            items = new ArrayList<>();
            temp = new Place("HELL",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
            //THE END
            exits = new ArrayList<>();
            items = new ArrayList<>();
            mons = new ArrayList<>();
            caras = new ArrayList<>();
            temp = new Place("THE_END",exits,items,mons,caras);
            GameMap.rooms.put(temp.getName(),temp);
            
        }
        public static void showMap()
        {
            GameMap.rooms.forEach((String k,Place v)->{
                System.out.println(k+"\n");
                v.display();
            });
        }
        public static Place getPlace(String place)
        {
            
            return GameMap.rooms.get(place);
        }

}