package general;


public abstract class Observable {

	protected final String DESC;

	/**
	 * 
	 * @param DESC
	 */
	public Observable(String DESC){
            this.DESC=DESC;
        }

	public String getDesc(){
            return this.DESC;
        }
        public abstract void display();
        public abstract void simpleDisplay();

}
