package place;

import general.Observable;
import item.Key;


public abstract class Exit extends Observable {

	private boolean isOpen;
	private boolean isLocked;
	private final String NAME;
        private final String DESTINATION;   


	/**
	 * 
	 * @param desc
	 * @param Name
	 * @param isOpen
	 * @param isLocked
	 */
	public Exit(String desc, String Name, boolean isOpen, boolean isLocked,String dest) {
		super(desc);
		this.NAME=Name;
                this.isLocked=isLocked;
                this.isOpen=isOpen;
                this.DESTINATION=dest;
	}
        
        protected boolean isOpen(){return this.isOpen;}
        protected boolean isLocked(){return this.isLocked;}
	public String getName() {
		return this.NAME;
	}
        public String getDestination() {
		return this.DESTINATION;
	}

	public abstract boolean open();

	protected void unLock(){
            this.isLocked=false;
        }
        //This function was added two days before the project was delivered. I am sure there are better solutions /with List<? extends Exit> but we dont have that much time to resolve this problem
        //to solve Key.useByHero 
        public abstract void keyUnLock(Key key);
        public abstract void simpleDisplay();
        

}