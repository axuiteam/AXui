package place;

import item.Key;

public class OpenExit extends Exit {

	private static final boolean DEFAULT_IS_OPEN = true;
	private static final boolean DEFAULT_IS_LOCKED = false;
        private static final String OPENEXIT_DESC="This is an openExit you just have to go through, it's always open"; 
	/**
	 * 
	 * @param name
	 * @param desc
         * @param dest
	 */
	public OpenExit(String name,String dest) {
		super(OPENEXIT_DESC,name,OpenExit.DEFAULT_IS_OPEN,OpenExit.DEFAULT_IS_LOCKED,dest);
	}

    /**
     *
     */
    @Override
	public boolean open() {
            if(!super.isLocked())
                return true;
            else{
                System.out.println("you can't open the door");
                return false;
            }
	}

       



    @Override
    public String getDesc() {
        return this.getClass().getSimpleName() +"\t"+ super.getDesc();
    }

    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Destination : "+this.getDestination());
        System.out.println("\t\t Description : "+this.getDesc());
        String str =(super.isLocked())?"LOCKED":"UNLOCKED";
        System.out.println("\t\t  "+str);
    }
    
        @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t Destination : "+this.getDestination());
        String str =(super.isLocked())?"LOCKED":"UNLOCKED";
        System.out.println("\t\t  "+str);
    }

    @Override
    public void keyUnLock(Key key) {
        System.out.println("it's open bro just go");
    }

}