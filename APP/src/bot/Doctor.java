
package bot;

import game.Hero;
import item.Heal;
import item.Item;
import java.util.List;
import java.util.Scanner;


public class Doctor extends Charactero implements Seller{
    private static final boolean IS_EVIL = false;
    private static final String DOCTOR_DESC="A doctor, helps you refill your healthpoint by selling you heals";
    
    public Doctor(String Name, int hp, List <Item> items) {
        super(Name,hp,DOCTOR_DESC,"DOCTOR :", IS_EVIL,items);   
       
        
    }
    
    @Override
    
     public void sellToHero( Hero hero){
        
      System.out.println("select your choice : ["+hero.getBalance()+"]");
        System.out.println("Index   NAME  Price");
        int i=1;
        for(Item e : this.items){
            System.out.println(i+" \t: "+ e.getClass().getSimpleName() +"   "+e.getValue() );
            i++;
        }
        
        
        Scanner sc = new Scanner(System.in);
        System.out.print("index >>");
        try{
                i=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    i=0;
                }
        i--;
        if(i>=0 && i < this.items.size())
        {
            if(hero.hasBalanceFor(this.items.get(i).getValue())){
                hero.getCurentRoom().addItem(this.items.get(i));
                hero.takeItem(this.items.get(i).getName());
                this.items.remove(i);
                System.out.println("DONE");
            }else{
                System.out.println("xxxxxxxxxx your insufficient balance xxxxxxxxxxxxxx");
            }
        }    
}

    @Override
    public void speackWithHero(Hero hero) {
        System.out.println("do you need medication ?");
        this.sellToHero(hero);
    }
}
