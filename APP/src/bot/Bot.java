
package bot;

import game.Hero;
import general.Observable;
import item.Item;
import java.util.ArrayList;
import java.util.List;
import place.Place;

public abstract class Bot extends Observable{
    
    private final String NAME;
    protected  int hp;
    private String MESSAGE;
    public List<Item> items;
    public Bot(String name, int hp, String DESC,String msg,List<Item> items) {
                super(DESC);
                this.MESSAGE=msg;
		this.NAME = name;
		this.hp = hp;
                this.items=new ArrayList();
                items.forEach(e->{this.items.add(e);});
                
	}
	
	public String getName() {
		return this.NAME;
	}
	
	public int getHp() {
		return this.hp;
	}
        public String getMessage(){
            return this.MESSAGE;
        }
	
	@Override 
        public String getDesc(){
            return DESC;
        }
        
        public void addItem(Item item)
        {
            this.items.add(item);
        }
	
        public void dropItems(Place place)
        {
            this.items.forEach(i->{place.addItem(i);System.out.println("item+");
                    });
        }
        
        public boolean isAlive()
        {
            if(this.hp>0){
            return true;
            }
            return false;
        }
        
        public boolean dead(Hero hero)
        {
            if(!this.isAlive())
            {
                this.dropItems(hero.getCurentRoom());
                hero.getCurentRoom().removeBot(this.getName());
                return true;
            }
            return false;
        }
        public void sideEffects(int Damage,Hero hero)
        {
            this.hp-=Damage;
            this.dead(hero);
        }
	public abstract void speackWithHero(Hero hero);
	
	public abstract void attackedByHero(Hero hero);
}
