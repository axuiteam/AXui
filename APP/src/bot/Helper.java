/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bot;

import game.Hero;
import java.util.ArrayList;




public class Helper extends Charactero {
    private static final boolean IS_EVIL = false;
    private static final String HELPER_DESC="A helper, can reveal some secrets in order to help you ";
    
    public Helper( String Name, int hp,String msg) {
        super(Name, hp,HELPER_DESC,msg,IS_EVIL,new ArrayList());   
  
    }
    
    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(this.getName()+" : "+this.getMessage());
    }
    
}
