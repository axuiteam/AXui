/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bot;

import item.Item;
import java.util.List;


public class Pig extends Monster {
    private static final int DEFAULT_HP = 35;
    private static final int DEFAULT_DAMAGE = 35;
    private static final String PigMonster_DESC="A pig, small with pink nose but can really hurt you.";
    
     public Pig( String Name,List<Item> items) {

        super(Name, DEFAULT_HP, PigMonster_DESC, DEFAULT_DAMAGE,items);
        
    }
}
