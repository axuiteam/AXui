/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bot;

import java.util.ArrayList;


public class GhostOfJustice extends Monster {
    private static final int DEFAULT_HP = 999999;
    private static final int DEFAULT_DAMAGE = 999999;
    private static final String GhostOfJustice_DESC="GhostOfJustice, justice is all I know so if you kill a good character, I'll take your life" ;
    public GhostOfJustice( String Name) {

        super(Name, DEFAULT_HP, GhostOfJustice_DESC, DEFAULT_DAMAGE,new ArrayList());
        
    }
    
   
}

