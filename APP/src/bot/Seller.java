
package bot;

import game.Hero;


public interface Seller {
    public void sellToHero(Hero hero); 
}
