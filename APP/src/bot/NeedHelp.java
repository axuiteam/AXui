
package bot;

import game.Hero;

import item.*;
import java.util.List;
import java.util.Scanner;
import java.util.Random;



public class NeedHelp extends Charactero {
    
    private static final String NeedHelp_DESC="A NeedHelp, I need help can you help me out?";
    
    
    private final int NEEDS; 
    
    public NeedHelp( String Name, int hp, int money,List<Item> items) {
        super(Name, hp,NeedHelp_DESC,"Can you help me?",NeedHelp.randomE(),items); 
        this.NEEDS = money; 
  
    }
    
    
    private static boolean randomE()
    {
        Random r=new Random();
        return r.nextBoolean();
    }
    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(this.getName()+" "+super.getMessage()+" i need "+this.NEEDS+"B : do you want give him");
        System.out.println("\t\t 1-yes");
        System.out.println("\t\t 2-no");
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print(">>");
                
                try{
                n=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    n=0;
                }
                System.out.println("");   
                switch(n)
                {   
                    case 1:{
                        if(hero.hasBalanceFor(this.NEEDS)){
                        hero.decreaseBalance(this.NEEDS);
                        if(!super.isEvil){
                            this.items.forEach(e->hero.getCurentRoom().addItem(e));
                            
                            System.out.println("Thanks");
                        }
                        hero.respectUp(this.NEEDS/10);
                        hero.getCurentRoom().removeBot(this.getName());
                     
                        }else{
                            System.out.println("xxxxxxxxxYOU CAN'T HELP HIM xxxxxxxxxxxx");
                        }
                           break;
                    }
                    case 2:{
                        
                        System.out.println("it's ok ):");
                        
                        break;
                    }
                    default:{
                        System.out.println("it's ok,):");
                        
                    }
                }
        
        
    }
    
}
