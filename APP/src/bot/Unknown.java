/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bot;

import game.Hero;
import item.Item;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ibra
 */
public class Unknown extends Charactero{
    
    private static final String UNKNOWN_DESC="Unknown, DONT TRUST ANY ONE";
    
    public Unknown( String Name, int hp,String msg,boolean isevil,List<Item> items) {
        super(Name, hp,UNKNOWN_DESC,msg,isevil,items);   
  
    }
    
    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(this.getName()+" : "+this.getMessage());
    }
}
