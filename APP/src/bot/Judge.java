/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bot;

import game.Hero;

import item.Item;
import java.util.List;


public class Judge extends Charactero {
    private static final boolean IS_EVIL = false;
    private static final int HP = 9999999;
    private static int THRESHOLD= 50;
    private static final String JUDGE_DESC= "A judge, if you want a key you have to pass my judgement ";
    
    public Judge( String Name, List<Item> objects) {
        super(Name, Judge.HP,JUDGE_DESC, "JUDGE : ",IS_EVIL,objects);   
        
    }  
    

    
    @Override
    public void speackWithHero(Hero hero) {
        if (hero.getRespect() > this.THRESHOLD){
            
            super.dropItems(hero.getCurentRoom());
            System.out.println("you deserve it ,key to the winnig room");
            hero.getCurentRoom().removeBot(this.getName());
        }
        else 
            System.out.println("i can't help you");
        
    }
}
