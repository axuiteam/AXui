
package bot;

import game.Hero;

import item.*;
import java.util.List;
import java.util.Scanner;



public class Trader extends Charactero implements Buyer,Seller {
    private static final boolean IS_EVIL = false;
    private static final String TRADER_DESC="A trader, sells items to you but can also buy yours"; 

    public Trader( String Name, int hp, List<Item> objects) {
        super(Name, hp,TRADER_DESC,"Trader",IS_EVIL,objects);   
        
    }   
    
    
    @Override    
    public void buyFromHero(Hero hero){
         System.out.println(super.getMessage()+" "+this.getName()+" : name what do you want sell");
         hero.getBag().getItms().forEach((k,v)->
         {
             System.out.println(k +":"+ v.getValue()/2 );

         });
         String s;
         Scanner sc = new Scanner(System.in);
         System.out.print(">>");
          try{
                s=sc.next();
                s=s.trim().toUpperCase();
                }
          catch(Exception e)
                {
                    sc.nextLine();
                    s="";
                }
          if(hero.getBag().hasItem(s))
          {
              
              hero.increaseBalance(hero.getBag().getItem(s).getValue()/2);
              this.items.add(hero.getBag().getItem(s));
              hero.getBag().removeItem(hero.getBag().getItem(s));
          }
          else
          {
              System.out.println("mmm ok bye");
          }
    }
    
    @Override
    public void sellToHero(Hero hero){
        System.out.println("select your choice : ["+hero.getBalance()+"]");
        System.out.println("Index   NAME  Price");
        int i=1;
        for(Item e : this.items){
            System.out.println(i+"\t : "+ e.getClass().getSimpleName() +"   "+e.getValue() );
            i++;
        }
        
        
        Scanner sc = new Scanner(System.in);
        System.out.print("index >>");
        try{
                i=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    i=0;
                }
        i--;
        if(i>=0 && i < this.items.size())
        {
            if(hero.hasBalanceFor(this.items.get(i).getValue())){
                hero.getCurentRoom().addItem(this.items.get(i));
                hero.takeItem(this.items.get(i).getName());
                this.items.remove(i);
                System.out.println("DONE");
            }else{
                System.out.println("xxxxxxxxxx your insufficient balance xxxxxxxxxxxxxx");
            }
        }
        
    }

    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(super.getMessage()+" "+this.getName()+" : select your choice");
        System.out.println("\t\t 1-Buy");
        System.out.println("\t\t 2-sell");
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print(">>");
                
                try{
                n=sc.nextInt();
                }catch(Exception e)
                {
                    sc.nextLine();
                    n=0;
                }
                System.out.println("");   
                switch(n)
                {   
                    case 1:{System.out.println("");
                        
                        this.sellToHero(hero);
                        
                        break;
                    }
                    case 2:{
                        this.buyFromHero(hero);
                        break;
                    }
                    default:{
                        System.out.println("mmm ok bye");
                        
                    }
                }
        
        
    }
    
    
}
