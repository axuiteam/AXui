
package bot;

import game.Hero;

public interface AttackHero {
    public void attackHero(Hero hero); 
}
