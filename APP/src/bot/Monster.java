
package bot;

import game.Hero;
import item.Item;
import java.util.List;


public class Monster extends Bot implements AttackHero{
    
    private int damage;
    private static final String MSG="ARGH ARGH ARGH GHHHHH";
    private static final String MONSTER_DESC="A monster, my ultimate objective is to kill you so show what you can do"; 
    public Monster(String name, int hp, String description, int damage,List<Item> items) {
        super(name, hp, description, Monster.MSG,items);
	this.damage = damage; 
    }
    public Monster(String name, int hp,  int damage,List<Item> items) {
        super(name, hp, MONSTER_DESC, Monster.MSG,items);
	this.damage = damage; 
    }
	
    public int getDamage() {
	return this.damage;
    }
    
	
    @Override 
    public void attackHero(Hero hero) {
        
        hero.decreaseHp_Shield(this.damage);
        System.out.println("\n\t/\\/\\"+this.getName()+" ATTACKS YOU ! HP[DAMAGE] "+this.hp+"["+this.damage+"]!!/\\/\\\n");
        System.out.println("\n\t/\\/\\ YOUR HP[SHIELD] :"+hero.getHp()+"["+hero.getShield()+"]!!/\\/\\\n");
    }
        
    @Override
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }

    @Override
    public void speackWithHero(Hero hero) {
        System.out.println(super.getName()+"\" "+super.getMessage()+"\"");
    }

    @Override
    public void attackedByHero(Hero hero) {
        super.hp -=hero.getDamage();
        if(this.dead(hero))
        {
            System.out.println("\n\t/\\/\\"+this.getName()+" KILLED !!/\\/\\\n");
        }
    }

    
        
}
 