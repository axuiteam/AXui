
package bot;

import game.Hero;
import item.Item;
import java.util.List;


public abstract class Charactero extends Bot {
    
    protected boolean isEvil; 
    public Charactero(String name, int hp, String DESC,String MSG, boolean isEvil,List<Item> items) {
       super(name, hp, DESC,MSG,items);
       this.isEvil = isEvil;
    }
    
    @Override
    
    public void display() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Description : "+this.getDesc());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }

    @Override
    public void simpleDisplay() {
        System.out.println(this.getClass().getSimpleName());
        System.out.println("\t\t Name : "+this.getName());
        System.out.println("\t\t HP : "+this.getHp());
    }



    @Override
    public void attackedByHero(Hero hero) {
        super.hp -=hero.getDamage();
        if(!this.isEvil)
            hero.respectDown(10);
        if(this.dead(hero)){
            hero.getCurentRoom().addMonster(new GhostOfJustice("jUsTiCe") );
        }
    }
    
}
