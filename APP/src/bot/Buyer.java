
package bot;

import game.Hero;

public interface Buyer {
    public void buyFromHero(Hero hero); 
}
